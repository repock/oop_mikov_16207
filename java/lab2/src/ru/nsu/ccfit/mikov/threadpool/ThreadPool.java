package ru.nsu.ccfit.mikov.threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPool {

    private int nThreads;
    private Queue<Runnable> taskQueue;
    private List<Thread> poolWorkers;

    public ThreadPool(int nThreads) {
        poolWorkers = new ArrayList<>(nThreads);
        this.nThreads = nThreads;
        taskQueue = new LinkedBlockingQueue<>();
        for (int i = 0; i < nThreads; i++) {
            poolWorkers.add(new PoolWorker());
            poolWorkers.get(i).start();
        }
    }

    public void execute(Runnable task) {
        synchronized (taskQueue) {
            taskQueue.add(task);
            taskQueue.notify();
        }
    }

    private class PoolWorker extends Thread {
        @Override
        public void run() {
            Runnable task;
            while (true) {
                synchronized (taskQueue) {
                    while (taskQueue.isEmpty()) {
                        try {
                            taskQueue.wait();
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    task = taskQueue.poll();
                }
                try {
                    task.run();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getQueueSize() {
        return taskQueue.size();
    }

    public void stop() {
        for (Thread thread : poolWorkers) {
            thread.interrupt();
        }
    }
}