package ru.nsu.ccfit.mikov.gui;


import ru.nsu.ccfit.mikov.factory.Factory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Window extends JFrame{
    Factory factory;
    Font infoFont = new Font("Arial", 17, 17);
    Font labelFont = new Font("Arial", 16, 16);

    private final int minDelay = 0;
    private final int maxDelay = 1000;
    private final int defaultDelay = (maxDelay + minDelay) / 2;

    private JLabel prodMotors = new JLabel("Произведено моторов");
    private JLabel prodBodys = new JLabel("Произведено кузовов");
    private JLabel prodAccessory = new JLabel("Произведено аксессуаров");
    private JLabel prodCars = new JLabel("Произведено машин");

    private JTextField prMotors = new JTextField();
    private JTextField prBodys = new JTextField();
    private JTextField prAccessorys = new JTextField();
    private JTextField prCars = new JTextField();

    private JLabel motors = new JLabel("Склад моторов");
    private JLabel bodys = new JLabel("Склад кузовов");
    private JLabel accessorys = new JLabel("Склад аксессуаров");
    private JLabel cars = new JLabel("Склад машин");

    private JTextField motorsInfo = new JTextField();
    private JTextField bodysInfo = new JTextField();
    private JTextField accessorysInfo = new JTextField();
    private JTextField carsInfo = new JTextField();

    private JLabel motorSpeed = new JLabel("Период производства моторов, мс");
    private JLabel bodySpeed = new JLabel("Период производства кузовов, мс");
    private JLabel accessorySpeed = new JLabel("Период производства аксессуаров, мс");
    private JLabel dealersSpeed = new JLabel("Период продажи машин, мс");

    private JSlider motorSlider = new JSlider(JSlider.HORIZONTAL, minDelay, maxDelay, defaultDelay);
    private JSlider bodySlider = new JSlider(JSlider.HORIZONTAL, minDelay, maxDelay, defaultDelay);
    private JSlider accessprySlider = new JSlider(JSlider.HORIZONTAL, minDelay, maxDelay, defaultDelay);
    private JSlider dealerSlider = new JSlider(JSlider.HORIZONTAL, minDelay, maxDelay, defaultDelay);

    public Window(Factory factory) {
        super();
        this.factory = factory;
        setResizable(false);
        setSize(700, 500);
        setLayout(new GridLayout(14, 2));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                factory.stop();
                System.exit(0);
            }
        });

        prodAccessory.setFont(labelFont);
        prodBodys.setFont(labelFont);
        prodMotors.setFont(labelFont);
        prodCars.setFont(labelFont);

        prAccessorys.setFont(infoFont);
        prAccessorys.setEditable(false);
        prAccessorys.setBackground(null);
        prAccessorys.setBorder(null);

        prBodys.setFont(infoFont);
        prBodys.setEditable(false);
        prBodys.setBackground(null);
        prBodys.setBorder(null);

        prMotors.setFont(infoFont);
        prMotors.setEditable(false);
        prMotors.setBackground(null);
        prMotors.setBorder(null);

        prCars.setFont(infoFont);
        prCars.setEditable(false);
        prCars.setBackground(null);
        prCars.setBorder(null);

        motors.setFont(labelFont);
        bodys.setFont(labelFont);
        accessorys.setFont(labelFont);
        cars.setFont(labelFont);

        motorsInfo.setFont(infoFont);
        motorsInfo.setEditable(false);
        motorsInfo.setBackground(null);
        motorsInfo.setBorder(null);

        bodysInfo.setFont(infoFont);
        bodysInfo.setEditable(false);
        bodysInfo.setBackground(null);
        bodysInfo.setBorder(null);

        accessorysInfo.setFont(infoFont);
        accessorysInfo.setEditable(false);
        accessorysInfo.setBackground(null);
        accessorysInfo.setBorder(null);

        carsInfo.setFont(infoFont);
        carsInfo.setEditable(false);
        carsInfo.setBackground(null);
        carsInfo.setBorder(null);

        motorSpeed.setFont(labelFont);
        bodySpeed.setFont(labelFont);
        accessorySpeed.setFont(labelFont);
        dealersSpeed.setFont(labelFont);

        add(prodMotors);
        add(prMotors);
        add(prodBodys);
        add(prBodys);
        add(prodAccessory);
        add(prAccessorys);
        add(prodCars);
        add(prCars);
        add(new JSeparator());
        add(new JSeparator());
        add(motors);
        add(motorsInfo);
        add(bodys);
        add(bodysInfo);
        add(accessorys);
        add(accessorysInfo);
        add(cars);
        add(carsInfo);
        add(new JSeparator());
        add(new JSeparator());

        motorSlider.setLabelTable(motorSlider.createStandardLabels(maxDelay));
        motorSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider) changeEvent.getSource();
                factory.setMotorDelay(source.getValue());
            }
        });
        motorSlider.setPaintLabels(true);
        bodySlider.setLabelTable(bodySlider.createStandardLabels(maxDelay));
        bodySlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider) changeEvent.getSource();
                factory.setBodyDelay(source.getValue());
            }
        });
        bodySlider.setPaintLabels(true);
        accessprySlider.setLabelTable(accessprySlider.createStandardLabels(maxDelay));
        accessprySlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider) changeEvent.getSource();
                factory.setAccessoryDelay(source.getValue());
            }
        });
        accessprySlider.setPaintLabels(true);
        dealerSlider.setLabelTable(dealerSlider.createStandardLabels(maxDelay));
        dealerSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider) changeEvent.getSource();
                factory.setDealerDelay(source.getValue());
            }
        });
        dealerSlider.setPaintLabels(true);

        add(motorSpeed);
        add(motorSlider);
        add(bodySpeed);
        add(bodySlider);
        add(accessorySpeed);
        add(accessprySlider);
        add(dealersSpeed);
        add(dealerSlider);

        setVisible(true);
        update();
        factory.setObserver(this);
    }

    public synchronized void update() {
        SwingUtilities.invokeLater(() -> {
            prMotors.setText(String.valueOf(factory.getProducedMotorsNum()));
            prBodys.setText(String.valueOf(factory.getProducedBodysNum()));
            prAccessorys.setText(String.valueOf(factory.getProducedAccessoryNum()));
            prCars.setText(String.valueOf(factory.getProducedCarsNum()));
            motorsInfo.setText(factory.getMotorNum() + "/" + factory.getMotorSize());
            bodysInfo.setText(factory.getBodyNum() + "/" + factory.getBodySize());
            accessorysInfo.setText(factory.getAccessoryNum() + "/" + factory.getAccessorySize());
            carsInfo.setText(factory.getCarNum() + "/" + factory.getCarSize());
        });
    }
}