package ru.nsu.ccfit.mikov;

import ru.nsu.ccfit.mikov.factory.Factory;
import ru.nsu.ccfit.mikov.gui.Window;

public class Main {
    public static void main(String[] args) {
        Factory factory = new Factory();
        Window window = new Window(factory);
    }
}
