package ru.nsu.ccfit.mikov.factory;


import ru.nsu.ccfit.mikov.gui.Window;

public class Supplier<I> implements Runnable {
    private int delay;
    private Class<I> itemClass;
    private Storage<I> itemStorage;
    private int itemsProduced = 0;
    private Window observer = null;
    private Thread thread;

    public Supplier(Class<I> itemClass, int delay, Storage storage) {
        this.itemStorage = storage;
        this.delay = delay;
        this.itemClass = itemClass;
        thread = new Thread(this, "supplier");
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            I newItem = null;
            try {
                newItem = itemClass.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            ++itemsProduced;
            if (observer != null) {
                observer.update();
            }
            try {
                itemStorage.addItem(newItem);
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getItemsProduced() {
        return itemsProduced;
    }

    public void setObserver(Window observer) {
        this.observer = observer;
    }

    public Thread getThread() {
        return thread;
    }
}