package ru.nsu.ccfit.mikov.factory;

import ru.nsu.ccfit.mikov.factory.items.Accessory;
import ru.nsu.ccfit.mikov.factory.items.Accessory;
import ru.nsu.ccfit.mikov.factory.items.Auto;
import ru.nsu.ccfit.mikov.factory.items.Body;
import ru.nsu.ccfit.mikov.factory.items.Motor;
import ru.nsu.ccfit.mikov.gui.Window;
import ru.nsu.ccfit.mikov.threadpool.ThreadPool;

public class Controller implements Runnable {
    private Storage<Body> bodyStorage;
    private Storage<Motor> motorStorage;
    private Storage<Accessory> accessoryStorage;
    private Storage<Auto> carStorage;
    private ThreadPool workers;
    private Window observer = null;
    private Thread thread;

    Controller(Storage<Body> bodyStorage, Storage<Motor> motorStorage,
               Storage<Accessory> accessoryStorage, Storage<Auto> carStorage, ThreadPool workers) {
        this.bodyStorage = bodyStorage;
        this.motorStorage = motorStorage;
        this.accessoryStorage = accessoryStorage;
        this.carStorage = carStorage;
        this.workers = workers;
        thread = new Thread(this, "controller");
        thread.start();
    }

    @Override
    public void run() {
        synchronized (carStorage.monitorGet) {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    return;
                }
                try {
                    carStorage.monitorGet.wait();
                } catch (InterruptedException e) {
                    return;
                }
                if (carStorage.getNumItems() < carStorage.getSize() / 5.0 && workers.getQueueSize() < 20) {
                    for (int i = 0; i < carStorage.getSize() / 5.0 ; i++) {
                        Worker worker = new Worker(bodyStorage, motorStorage, accessoryStorage, carStorage);
                        if (observer != null) {
                            worker.setObserver(observer);
                        }
                        workers.execute(worker);
                    }
                }
            }
        }
    }

    public void setObserver(Window observer) {
        this.observer = observer;
    }

    public Thread getThread() {
        return thread;
    }
}