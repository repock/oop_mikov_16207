package ru.nsu.ccfit.mikov.factory;

import ru.nsu.ccfit.mikov.factory.items.Accessory;
import ru.nsu.ccfit.mikov.factory.items.Auto;
import ru.nsu.ccfit.mikov.factory.items.Body;
import ru.nsu.ccfit.mikov.factory.items.Motor;
import ru.nsu.ccfit.mikov.gui.Window;

public class Worker implements Runnable{

    private Storage<Body> bodyStorage;
    private Storage<Motor> motorStorage;
    private Storage<Accessory> accessoryStorage;
    private Storage<Auto> carStorage;
    private static int carsProduced = 0;
    private static final Object monitor = new Object();
    private Window observer = null;

    private Auto makeCar(Body body, Motor motor, Accessory accessory) {
        synchronized (monitor) {
            ++carsProduced;
        }
        return new Auto(motor, body, accessory);
    }

    public Worker(Storage<Body> bodyStorage, Storage<Motor> motorStorage,
                  Storage<Accessory> accessoryStorage, Storage<Auto> carStorage) {
        this.bodyStorage = bodyStorage;
        this.motorStorage = motorStorage;
        this.carStorage = carStorage;
        this.accessoryStorage = accessoryStorage;
    }

    @Override
    public void run() {
        try {
            Body body = bodyStorage.getItem();
            Motor motor = motorStorage.getItem();
            Accessory accessory = accessoryStorage.getItem();
            Auto car = makeCar(body, motor, accessory);
            if (observer != null) {
                observer.update();
            }
            carStorage.addItem(car);
        } catch (InterruptedException e) {
        }
    }

    public static int getCarsProduced() {
        return carsProduced;
    }

    public void setObserver(Window observer) {
        this.observer = observer;
    }
}