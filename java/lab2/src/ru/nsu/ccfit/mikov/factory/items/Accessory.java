package ru.nsu.ccfit.mikov.factory.items;

public class Accessory extends Item {
    public Accessory() {
        super();
    }

    public String getId() {
        return "AC" + id;
    }
}
