package ru.nsu.ccfit.mikov.factory.items;

public class Motor extends Item {
    public Motor() {
        super();
    }

    public String getId() {
        return "MT" + id;
    }
}
