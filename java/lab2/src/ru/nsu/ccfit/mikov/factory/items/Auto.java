package ru.nsu.ccfit.mikov.factory.items;

public class Auto extends Item {
    private Body body;
    private Motor motor;
    private Accessory accessory;
    public Auto(Motor motor, Body body, Accessory accessory) {
        super();
        this.body = body;
        this.accessory = accessory;
        this.motor = motor;
    }

    public String getBodyId() {
        return body.getId();
    }

    public String getMotorId() {
        return motor.getId();
    }

    public String getAccessoryId() {
        return accessory.getId();
    }

    public String getId() {
        return "AU" + id;
    }
}