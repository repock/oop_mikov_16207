package ru.nsu.ccfit.mikov.factory;

import ru.nsu.ccfit.mikov.factory.items.Accessory;
import ru.nsu.ccfit.mikov.factory.items.Auto;
import ru.nsu.ccfit.mikov.factory.items.Body;
import ru.nsu.ccfit.mikov.factory.items.Motor;
import ru.nsu.ccfit.mikov.gui.Window;
import ru.nsu.ccfit.mikov.threadpool.ThreadPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Factory {
    private final String propFile = "/ru/nsu/ccfit/mikov/factory/config.properties";
    private final String filename = "log.txt";
    private int motorDelay = 500;
    private int bodyDelay = 500;
    private int accessoryDelay = 500;
    private int dealerDelay = 500;
    private int workersNum;
    private int dealersNum;
    private int suppliersNum;
    private Properties config = new Properties();
    private Storage<Body> bodyStorage;
    private Storage<Motor> motorStorage;
    private Storage<Accessory> accessoryStorage;
    private Storage<Auto> carStorage;
    private List<Supplier<Accessory>> accessorySuppliers;
    private Supplier<Motor> motorSupplier;
    private Supplier<Body> bodySupplier;
    private ThreadPool workers;
    private Controller controller;
    private List<Dealer> dealers;
    private Window observer = null;


    public Factory() {
        try {
            config.load(Factory.class.getResourceAsStream(propFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        bodyStorage = new Storage<>(Integer.valueOf(config.getProperty("BodyStorageSize")));
        motorStorage = new Storage<>(Integer.valueOf(config.getProperty("MotorStorageSize")));
        accessoryStorage = new Storage<>(Integer.valueOf(config.getProperty("AccessoryStorageSize")));
        carStorage = new Storage<>(Integer.valueOf(config.getProperty("AutoStorageSize")));
        suppliersNum = Integer.valueOf(config.getProperty("AccessorySuppliers"));
        accessorySuppliers = new ArrayList<>(suppliersNum);
        for (int i = 0; i < suppliersNum; i++) {
            accessorySuppliers.add(new Supplier<>(Accessory.class, accessoryDelay, accessoryStorage));
        }
        motorSupplier = new Supplier<>(Motor.class, motorDelay, motorStorage);
        bodySupplier = new Supplier<>(Body.class, bodyDelay, bodyStorage);
        workersNum = Integer.valueOf(config.getProperty("Workers"));
        workers = new ThreadPool(workersNum);
        controller = new Controller(bodyStorage, motorStorage, accessoryStorage, carStorage, workers);
        dealersNum = Integer.valueOf(config.getProperty("Dealers"));
        dealers = new ArrayList<>(dealersNum);
        Dealer.openWriter(filename);
        for (int i = 0; i < dealersNum; i++) {
            dealers.add(new Dealer(carStorage, dealerDelay, i + 1));
        }
    }

    public void setAccessoryDelay(int accessoryDelay) {
        this.accessoryDelay = accessoryDelay;
        for(Supplier<Accessory> supplier : accessorySuppliers) {
            supplier.setDelay(accessoryDelay);
        }
    }

    public void setBodyDelay(int bodyDelay) {
        this.bodyDelay = bodyDelay;
        bodySupplier.setDelay(bodyDelay);
    }

    public void setMotorDelay(int motorDelay) {
        this.motorDelay = motorDelay;
        motorSupplier.setDelay(motorDelay);
    }

    public void setDealerDelay(int dealerDelay) {
        this.dealerDelay = dealerDelay;
        for (Dealer dealer :dealers) {
            dealer.setDelay(dealerDelay);
        }
    }

    public void setObserver(Window observer) {
        this.observer = observer;
        carStorage.setObserver(observer);
        bodyStorage.setObserver(observer);
        motorStorage.setObserver(observer);
        accessoryStorage.setObserver(observer);
        controller.setObserver(observer);
        motorSupplier.setObserver(observer);
        bodySupplier.setObserver(observer);
        for (Supplier<Accessory> supplier : accessorySuppliers) {
            supplier.setObserver(observer);
        }
    }

    public void stop() {
        Dealer.closeWriter();
        for (Dealer dealer : dealers) {
            dealer.getThread().interrupt();
        }
        for (Supplier<Accessory> supplier : accessorySuppliers) {
            supplier.getThread().interrupt();
        }
        bodySupplier.getThread().interrupt();
        motorSupplier.getThread().interrupt();
        controller.getThread().interrupt();
        workers.stop();
    }

    public int getAccessoryDelay() {
        return accessoryDelay;
    }

    public int getBodyDelay() {
        return bodyDelay;
    }

    public int getMotorDelay() {
        return motorDelay;
    }

    public int getAccessoryNum() {
        return accessoryStorage.getNumItems();
    }

    public int getMotorNum() {
        return  motorStorage.getNumItems();
    }

    public int getBodyNum() {
        return bodyStorage.getNumItems();
    }

    public int getCarNum() {
        return carStorage.getNumItems();
    }

    public int getAccessorySize() {
        return accessoryStorage.getSize();
    }

    public int getMotorSize() {
        return  motorStorage.getSize();
    }

    public int getBodySize() {
        return bodyStorage.getSize();
    }

    public int getCarSize() {
        return carStorage.getSize();
    }

    public int getDealerDelay() {
        return dealerDelay;
    }

    public int getProducedCarsNum() {
        return Worker.getCarsProduced();
    }

    public int getProducedMotorsNum() {
        return motorSupplier.getItemsProduced();
    }

    public int getProducedBodysNum() {
        return bodySupplier.getItemsProduced();
    }

    public int getProducedAccessoryNum() {
        int num = 0;
        for (Supplier<Accessory> supplier : accessorySuppliers) {
            num += supplier.getItemsProduced();
        }
        return num;
    }

}
