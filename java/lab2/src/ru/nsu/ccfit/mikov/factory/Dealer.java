package ru.nsu.ccfit.mikov.factory;


import ru.nsu.ccfit.mikov.factory.items.Auto;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;

public class Dealer implements Runnable{
    private int delay;
    private Storage<Auto> carStorage;
    private static Writer writer;
    private int number;
    private final static Object monitor = new Object();
    private Thread thread;

    private void sale(Auto car) {
        try {
            synchronized (monitor) {
                if (writer != null) {
                    writer.write(LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + ":" +
                            LocalDateTime.now().getSecond() + ":Dealer" + number + ":Auto:" +
                            car.getId() + "(Body:" + car.getBodyId() + ",Motor:" + car.getMotorId() + ",Accessory:" + car.getAccessoryId() + ")\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Dealer(Storage<Auto> carStorage, int delay, int number) {
        this.carStorage = carStorage;
        this.delay = delay;
        this.number = number;
        thread = new Thread(this, "dealer");
        thread.start();
    }

    @Override
    public void run() {
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            try {
                Auto car = carStorage.getItem();
                sale(car);
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getDelay() {
        return delay;
    }

    public Thread getThread() {
        return thread;
    }

    public static void openWriter(String filename) {
        try {
            writer = new FileWriter(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void closeWriter() {
        try {
            writer.close();
            writer = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}