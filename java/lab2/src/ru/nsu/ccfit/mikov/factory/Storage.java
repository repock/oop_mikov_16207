package ru.nsu.ccfit.mikov.factory;


import ru.nsu.ccfit.mikov.gui.Window;

import java.util.Stack;

public class Storage<I> {
    private int size;
    private Stack<I> items;
    final public Object monitorGet = new Object();
    final public Object monitor = new Object();
    private Window observer = null;

    public Storage(int size) {
        this.size = size;
        items = new Stack<>();
    }

    public void addItem(I item) throws InterruptedException {
        synchronized (monitor) {
            while (items.size() >= size) {
                monitor.wait();
            }
            items.push(item);
            if (observer != null) {
                observer.update();
            }
            monitor.notifyAll();
        }
    }

    public I getItem() throws InterruptedException {
        synchronized (monitor) {
            synchronized (monitorGet) {
                monitorGet.notifyAll();
            }
            while (items.size() <= 0) {
                monitor.wait();

            }
            I item = items.pop();
            if (observer != null) {
                observer.update();
            }
            monitor.notifyAll();
            return item;
        }
    }

    public void setObserver(Window observer) {
        this.observer = observer;
    }

    public int getNumItems() {
        return items.size();
    }

    public int getSize() {
        return size;
    }

}
