package ru.nsu.mikov.lab1_2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class LabOneS {
        public static void main(String argv[]) {
            if (argv.length < 1) {
                System.err.println("Few arguments, min 1");
                return;
            }

            String name;
            if(argv.length > 1){
                name = argv[1];
            }
            else {
                name = "output";
            }

            AtomicInteger allWorldCount = new AtomicInteger();
            StringBuffer sb = new StringBuffer();

            try (Writer out = new OutputStreamWriter(new FileOutputStream(name))) {
                Files.lines(Paths.get(argv[0]))
                        .map(line -> line.split("\\W+"))
                        .flatMap(Arrays::stream)
                        .filter(line -> !line.isEmpty())
                        .peek(val -> allWorldCount.getAndIncrement())
                        .collect(groupingBy(identity(), counting()))
                        .entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .forEach((Map.Entry<String, Long> it) -> {
                            try {
                                sb.append(it.getKey()).append(", ").append(it.getValue()).append(", ").append("%").append(((float)it.getValue() * 100 / allWorldCount.get())).append("\n");
                                out.write(sb.toString());
                                sb.setLength(0);
                            }
                            catch (FileNotFoundException  except) {
                                except.printStackTrace();
                            }
                            catch (IOException except) {
                                except.printStackTrace();
                            }
                        });
            }
            catch (RuntimeException  | IOException except) {
                System.err.println(except);
            }
        }
}
