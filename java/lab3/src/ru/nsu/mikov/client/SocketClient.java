package ru.nsu.mikov.client;

import ru.nsu.mikov.server.Message;
import ru.nsu.mikov.server.Ping;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class SocketClient {
    private final static String address = "192.168.0.105";
    private final static int serverPort = 33822;

    private static String userName = "";
    static Socket socket = null;
    public static void main( String[] args ) {
        System.out.println("Welcome to the chat client!");
        System.out.println("Enter your nickname and click \"Enter\"");

// Создаем поток для чтения с клавиатуры
        BufferedReader keyboard = new BufferedReader( new InputStreamReader( System.in ) );
        try {
// Ждем пока пользователь введет свой ник и нажмет кнопку Enter
            userName = keyboard.readLine();
            System.out.println();
        } catch ( IOException e ) { e.printStackTrace(); }

        try {
            try {
                InetAddress ipAddress = InetAddress.getByName( address ); // создаем объект который отображает вышеописанный IP-адрес
                socket = new Socket( ipAddress, serverPort ); // создаем сокет используя IP-адрес и порт сервера

// Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом
                InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();


                ObjectOutputStream objectOutputStream = new ObjectOutputStream( outputStream );
                ObjectInputStream objectInputStream = new ObjectInputStream( inputStream );

                //new PingThread( objectOutputStream, objectInputStream );

// Создаем поток для чтения с клавиатуры
                String message = null;
                System.out.println("Type the message and press \"Enter\"");

                while (true) {
                    message = keyboard.readLine();
                    objectOutputStream.writeObject( new Message( userName, message ) ); // отсылаем введенную строку текста серверу.
                }
            } catch ( Exception e ) { e.printStackTrace(); }
        }
        finally {
            try {
                if ( socket != null ) { socket.close(); }
            } catch ( IOException e ) { e.printStackTrace(); }
        }
    }
}

