package ru.nsu.mikov.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server {

    private static UsersList list = new UsersList();
    private static ChatHistory chatHistory = new ChatHistory();

    public static void main(String[] args) {
        try{
            //слушатель
            ServerSocket socketListener = new ServerSocket(33822);

            while(true) {
                Socket client = null;
                while (client == null){
                    client = socketListener.accept();
                }
                //новый поток, которому передаем наш сокет
                new ClientThread(client);
            }
        } catch (SocketException e) {
            System.err.println("Socket exception");
            e.printStackTrace();
        }catch (IOException e) {
            System.err.println("IO exception");
        }
    }
    public synchronized static UsersList getUserList() {
        return list;
    }

    public synchronized static ChatHistory getChatHistory() {
        return chatHistory;
    }
}
