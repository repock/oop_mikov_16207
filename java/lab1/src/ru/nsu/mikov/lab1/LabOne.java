package ru.nsu.mikov.lab1;

import java.util.*;
import java.io.PrintWriter;
/*
разнести по классам вывод и подсчет слов и сделать 2 вариант
 */
public class LabOne {
    public static void main(String argv[]) {
        if (argv.length < 1) {
            System.err.println("Few arguments, min 1");
            return;
        }

        String name;
        if(argv.length > 1){
            name = argv[1];
        }
        else {
            name = "output";
        }

        try (Scanner scan = new Scanner(new java.io.FileReader(argv[0]));
             PrintWriter fout =  new PrintWriter(name)) {

            Map<String, Integer> stringMap = new HashMap<>();

            int allWorldCount = 0;

            scan.useDelimiter("\\W+");

            while (scan.hasNext()) {
                stringMap.merge(scan.next(), 1, (inc, current_key) -> inc + current_key);
                allWorldCount++;
            }

            List<Map.Entry<String, Integer>> entryArray = new ArrayList<>();

            entryArray.addAll(stringMap.entrySet());

            entryArray.sort(Map.Entry.comparingByValue((a, b) -> b - a));//ошибки переполнения

            for (Map.Entry<String, Integer> entry : entryArray) {
                fout.printf("%s, %d, %%%f\n", entry.getKey(), entry.getValue(), (double)(100 * entry.getValue()) / allWorldCount);
            }

        }
        catch (RuntimeException | java.io.FileNotFoundException except) {
            System.err.println(except);
        }
    }
}
