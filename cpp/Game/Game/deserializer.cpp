#include "deserializer.h"

void Deserializer::load(std::vector<std::vector<Cell>>&field, ConsolView &view, std::map<int, string> &history_map, string filename)
{
	string temp_string1;
	ifstream fin(filename);

	if (!fin.is_open())
		throw exception("Can't open this file");
	
	getline(fin, temp_string1);
	
	if (temp_string1 != "ABCDEFGHIJ")
	{
		//iSSUE �� ����������� �����
		fin.close();
		throw exception("BAD LETTERS, file is not our game or damaged");
	}
	
	for (int i = 0; i < field.size(); i++)
	{
		getline(fin, temp_string1);
		if (temp_string1[0] <= '9' && temp_string1[0] >= '0')
		{
			for (int j = 1; j <= field[0].size(); j++)
			{
				if (temp_string1[j] != '.' && temp_string1[j] != '*')
				{
					fin.close();
					throw exception("BAD CELLS, file is not our game or damaged");
				}
				if (temp_string1[j] == '.')
					field[i][j - 1].set_state(Dead);
				else
					field[i][j - 1].set_state(Alive);
			}
		}
		else
		{
			fin.close();
			throw exception("BAD NUMBER, file is not our game or damaged");
		}
	}

	view.counter = 0;
	history_map.clear();
	fin.close();
}
