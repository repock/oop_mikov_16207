﻿#pragma once

#include <string>
#include <iostream>
#include <map>
#include <list>

using namespace std;

class Parser
{
public:
	enum command { step, save, load, set, clear, reset, back, exit, help };
	
	Parser();

	pair<enum command, list<string>> pars();
	void split();

private:
	std::map<std::string, command> command_map;
	string command;
	string argumentXY;

	std::list<string> pars_coords(string);
	std::list<string> pars_filename(string);
	std::list<string> pars_step_num(string);
};