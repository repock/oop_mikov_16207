﻿#include "cell.h"

Cell::Cell()
{
	cur_state = Dead;
}

void Cell::set_neighbours(int neigh)
{
	neighbours = neigh;
}

int Cell::get_neighbours()
{
	return neighbours;
}

bool Cell::get_state()
{
	return cur_state;
}

void Cell::set_state(Life state)
{
	cur_state = state;
}