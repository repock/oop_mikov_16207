﻿#pragma once

#include "cell.h"
//#include "field.h"
#include "parser.h"
//#include "executor.h"
#include "consolView.h"

class Strategy
{
public:
	Strategy(std::vector<std::vector<Cell>>& fld, std::map<int, std::string>& hist);

	void set(int, int);
	void clear(int, int);
	void reset(ConsolView &);
	void step(ConsolView &, int);
	void back(ConsolView &);

private:
	//ISSUE добавить поля история, поле
	std::vector<vector<Cell>> & field;
	std::map<int, string> & history_map;

	std::string get_field_string();
	void set_field_string(int);
	void update_round();
	void steprul(int, int);
};
