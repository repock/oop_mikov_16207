﻿#pragma once

#include <iostream>
#include <istream>
#include <windows.h>
#include <fstream>
using namespace std;

enum Life { Dead, Alive };


class Cell
{
private:
	bool cur_state;
	int neighbours = 0;

public:
	Cell();

	void set_neighbours(int);
	int get_neighbours();

	void set_state(Life);
	bool get_state();
};
