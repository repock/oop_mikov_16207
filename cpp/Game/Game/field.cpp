﻿#include "field.h"

Field::Field()
{
	field.resize(height);

	for (size_t i = 0; i < height; i++)
	{
		field[i].resize(width);
	}
}

std::vector<vector<Cell>>& Field::get_field()
{
	return field;
}