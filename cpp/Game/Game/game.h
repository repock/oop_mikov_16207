﻿#pragma once

#include "consolView.h"
#include "parser.h"
#include "executor.h"

class Game
{
private:
	ConsolView view;
	Executor *executor;
	Field field;
public:
	Game();
	~Game();

	void start();
};
