#pragma once

#include "cell.h"
#include <vector>

class Serializer
{
public:
	static void save(std::vector<std::vector<Cell>> &, string);
};