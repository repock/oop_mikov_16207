#include "serializer.h"

void Serializer::save(std::vector<std::vector<Cell>>&field, string filename)
{
	ofstream fout(filename);
	
	if (!fout.is_open())
	{
		throw exception("Can't open this file");
	}

	for (int i = 0; i < field[0].size(); i++)
	{
		char letter = i + 65;
		fout << letter;
	}

	fout << endl;

	for (int i = 0; i < field.size(); i++)
	{
		fout << i;
		for (int j = 0; j < field[0].size(); j++)
		{
			if (field[i][j].get_state())
				fout << "*";
			else 
				fout << ".";
		}
		fout << endl;
	}

	fout.close();
}