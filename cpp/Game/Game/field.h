﻿#pragma once

#include "cell.h"
#include <vector>

class Field
{
private:
	size_t width = 10;
	size_t height = 10;
	std::vector<vector<Cell>> field;

public:
	Field();
	std::vector<vector<Cell>> & get_field();
};
