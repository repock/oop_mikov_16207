﻿#pragma once

#include "cell.h"
#include "field.h"
#include "strategy.h"
#include "consolView.h"
#include <list>
#include "serializer.h"
#include "deserializer.h"

class Executor
{
private:
	enum command { step, save, load, set, clear, reset, back, exit, help };
	std::map<int, string> history_map;
	Strategy *strategy;
	Parser parser_;
	std::vector<std::vector<Cell>> &field;

public:
	Executor(std::vector<vector<Cell>> &);
	~Executor();

	bool commands(ConsolView &);
};