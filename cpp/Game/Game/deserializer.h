#pragma once

#include "cell.h"
#include "consolView.h"
#include <vector>
#include <string>
#include <map>

class Deserializer
{
public:
	static void load(std::vector<std::vector<Cell>> &, ConsolView &, std::map<int, string> &, string);
};