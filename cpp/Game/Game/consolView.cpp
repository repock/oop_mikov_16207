﻿#include "consolView.h"

ConsolView::ConsolView()
{
}

void ConsolView::help()
{
	setlocale(LC_ALL, "Russian");
	std::cout << "ПРАВИЛА:" << std::endl;
	std::cout << "1.Игра идет по шагам" << std::endl;
	std::cout <<" 2.Игра ведется на тороидальном поле 10 на 10 (координаты от A - J, 0 - 9) соседями верхних(горизонталь 9) клеток являются нижние(горизонталь 0), соседями правых клеток(вертикаль J) являются левые(вертикаль A)" << std::endl;
	std::cout << "3.Каждая клетка имеет 8 соседей(по горизонтали и диагонали)"<< std::endl;
	std::cout << "4.Если у пустой(мертвой)  клетки ровно три соседа - организма(живые клетки), то на следующий ход в ней зарождается жизнь(организм)"<< std::endl;
	std::cout << "5.Если у живой клетки менее двух живых соседей то она умирает от одиночества"<< std::endl;
	std::cout << "6.Если у живой клетки более трех живых соседей то она умирает от перенаселения"<< std::endl;
	std::cout << "КОМАНДЫ:" << std::endl;
	std::cout << "1.reset – очистить поле и счетчик ходов" << std::endl;
	std::cout << "2.set XY (где X – от A до J, Y от 0 до 9) — установить организм в клетку" << std::endl;
	std::cout << "3.clear XY – очистить клетку" << std::endl;
	std::cout << "4.step N – прокрутить игру вперед на N шагов (может отсутствовать, тогда полагается равным 1)" << std::endl;
	std::cout << "5.back  – прокрутить игру назад на один шаг" << std::endl;
	std::cout << "6.save “filename”  – сохранить состояние поля игры в текстовый файл в текущей директории (без истории ходов)" << std::endl;
	std::cout << "7.load “filename”  – загрузить состояние поля игры из текстового файла в текущей директории (с обнулением счетчика ходов)" << std::endl;
}

void ConsolView::draw(std::vector<vector<Cell>> &field) const
{
	cout << "  ";

	for (int i = 0; i < field[0].size(); i++)
	{
		char letter = i + 65;
		cout << letter << " ";
	}
	
	cout << endl;
	
	for (int i = 0; i < field.size(); i++)
	{
		cout << i << " ";
		for (int j = 0; j < field[0].size(); j++)
		{
			if (field[i][j].get_state() == true)
				cout << "* ";
			else cout << ". ";
		}
		cout << endl;
	}
	cout << "COUNTER " << counter << endl;
}
