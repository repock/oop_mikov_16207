﻿#include "game.h"

Game::Game()
{
	view.counter = 0;
	executor = new Executor(field.get_field());
}

Game::~Game()
{
	delete executor;
}

void Game::start()
{
	bool next_turn = true;

	while(next_turn)
	{
		try
		{
			system("cls");
			view.draw(field.get_field());
			next_turn = executor->commands(view);
		}
		catch (exception &ex)
		{
			cout << ex.what() << endl;
			cout << "Use <help> for detailed information about the commands" << endl;
			system("pause");
		}
	}
}
