﻿#pragma once

// help
#include <iostream>
#include <locale.h>

#include "cell.h"
#include <vector>

class ConsolView
{
public:
	int counter;
	int back_counter = 0;

	ConsolView();
	void help();
	void draw(std::vector<vector<Cell>> &) const;
};
