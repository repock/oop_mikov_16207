﻿#include "executor.h"

Executor::Executor(std::vector<std::vector<Cell>> &fld) :
	field(fld)
{
	std::string def_field = "";

	for (int i = 0; i < 100; i++)
		def_field += '0';

	history_map.insert({ 0, def_field });
	strategy = new Strategy(field, history_map);
}

Executor::~Executor()
{
	delete strategy;
}

bool Executor::commands(ConsolView &view)
{
	parser_.split();
	std::pair<enum Parser::command, list<string>> command_pair = parser_.pars();

	switch(command_pair.first)
	{
	case set:
		strategy->set( atoi(command_pair.second.front().c_str()), atoi(command_pair.second.back().c_str()));
		break;

	case clear:
		strategy->clear(atoi(command_pair.second.front().c_str()), atoi(command_pair.second.back().c_str()));
		break;
	
	case reset:
		strategy->reset(view);
		break;

	case step:
		strategy->step(view, atoi(command_pair.second.front().c_str()));
		break;

	case back:
		strategy->back(view);
		break;

	case save:
		Serializer::save(field, command_pair.second.front());
		break;

	case load:
		Deserializer::load(field, view, history_map, command_pair.second.front());
		break;

	case help:
		view.help();
		break;

	case exit:
		return false;
	}

	return true;
}

