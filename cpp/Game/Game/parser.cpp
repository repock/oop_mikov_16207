﻿#include "parser.h"

Parser::Parser()
{
	command_map = { { "step" , step },
	{ "save" , save },
	{ "load" , load },
	{ "set"  , set },
	{ "clear", clear },
	{ "reset", reset },
	{ "back" , back },
	{ "exit" , exit },
	{ "help" , help } };
}

void Parser::split()
{
	string temp_command;
	getline(cin, temp_command);

	size_t iter = 0;

	while (iter < temp_command.length())
	{
		if (temp_command[iter] == ' ' || temp_command[iter] == '\t' || temp_command[iter] == '\n')
			break;
		iter++;
	}
	command = temp_command.substr(0, iter);

	if (iter + 1 <= temp_command.length())
		argumentXY = temp_command.substr(iter + 1);
	else
		argumentXY = "";
}

std::list<string> Parser::pars_coords(string args_line)
{
	if (args_line.length() != 2)
		throw exception("BAD ARGUMEN, example: <set/clear> [A-J][0-9]");

	int Ox = 0;
	int Oy = 0;

	if (args_line[0] >= 'A' && args_line[0] <= 'J' && args_line[1] >= '0' && args_line[1] <= '9')
	{
		Oy = args_line[0] - 'A';
		Ox = args_line[1] - '0';
	}
	else
		throw exception("BAD ARGUMENTS, example: <set/clear> [A-J][0-9]");

	std::list<string> com_list;
	com_list.push_back(std::to_string(Ox));
	com_list.push_back(std::to_string(Oy));

	return com_list;
}

std::list<string> Parser::pars_filename(string filename)
{
	std::list<string> com_list;
	com_list.push_back(filename);
	return com_list;
}

std::list<string> Parser::pars_step_num(string args_line)
{
	std::list<string> com_list;

	if (args_line.length() == 0)
	{
		com_list.push_back(std::to_string(1));
		return com_list;
	}

	int steps = 0;

	for (size_t i = 0; i < args_line.length(); i++)
	{
		if (args_line[i] < '0' || args_line[i] > '9')
			throw exception("BAD ARGUMENTS, example: step or step [0-...] (one step or more)");

		steps = steps * 10 + args_line[i] - '0';
	}

	com_list.push_back(std::to_string(steps));

	return com_list;
}

pair<enum Parser::command, list<string>> Parser::pars()
{
	int value = 0;
	string paremeters = "";

	for (size_t iter = 0; iter < argumentXY.length(); iter++)
	{
		if (argumentXY[iter] != ' ' && argumentXY[iter] != '\t')
		{
			paremeters += argumentXY[iter];
		}

		if (argumentXY[iter] != ' ' && argumentXY[iter + 1] == ' ')
		{
			value++;
		}
	}

	enum command com;

	try
	{
		com = command_map.at(command);
	}
	catch(std::exception &ex)
	{
		std::cout << "Unknown command: " + command << std::endl;
		throw;
	}

	if (com == set || com == clear)
	{
		if (value <= 1)
		{
			argumentXY = paremeters.substr(0);
			return make_pair(com, pars_coords(argumentXY));
		}

		throw (std::out_of_range(command));
	}

	if (com == step)
	{
		if (value <= 1)
		{
			argumentXY = paremeters.substr(0);
			return make_pair(com, pars_step_num(argumentXY));
		}

		throw (std::out_of_range(command));
	}

	if (com == save || com == load)
	{
		if (value <= 1)
		{
			argumentXY = paremeters.substr(0);
			return make_pair(com, pars_filename(argumentXY));
		}

		throw (std::out_of_range(command));
	}

	if (com == reset || com == back || com == exit || com == help)
	{
		if (value == 0)
		{
			argumentXY = paremeters.substr(0);
			return std::make_pair(com, pars_filename(""));
		}

		throw (std::out_of_range(command));
	}
}

