﻿#include "strategy.h"

Strategy::Strategy(std::vector<std::vector<Cell>> &fld, std::map<int, std::string> &hist) :
	field(fld),
	history_map(hist)
{
}

void Strategy::set(int x, int y)
{
	field[x][y].set_state(Alive);
}

void Strategy::clear(int x, int y)
{
	field[x][y].set_state(Dead);
}

void Strategy::reset(ConsolView &view)
{
	view.counter = 0;

	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			field[i][j].set_state(Dead);
		}
	}
}

void Strategy::step(ConsolView &view, int steps)
{
	for (int i = 0; i < steps; i++)
	{
		view.counter++;
		update_round();

		for (int i = 0; i < field.size(); i++)
			for (int j = 0; j < field[0].size(); j++)
				steprul(i, j);

		history_map.insert({ view.counter, get_field_string() });
	}
}

void Strategy::update_round()
{
	int cur_neighbours = 0;

	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			for (int OY = -1; OY < 2; OY++)
			{
				for (int OX = -1; OX < 2; OX++)
				{
					if (OX != 0 || OY != 0)
					{
						cur_neighbours += field[(i + OY + field.size()) % field.size()][(j + OX + field[0].size()) % field[0].size()].get_state();
					}
				}
			}

			field[i][j].set_neighbours(cur_neighbours);
			cur_neighbours = 0;
		}
	}
}

void Strategy::steprul(int x, int y)
{
	if (field[y][x].get_neighbours() == 3)
		field[y][x].set_state(Alive);
	
	if (field[y][x].get_neighbours() > 3)
		field[y][x].set_state(Dead);
	
	if (field[y][x].get_neighbours() < 2)
		field[y][x].set_state(Dead);
}

void Strategy::back(ConsolView &view)
{
	if (view.counter > 0)
	{
		set_field_string(view.counter);
		view.counter--;
	}
	else
	{
		throw (std::exception("Impossible back"));
	}
}

std::string Strategy::get_field_string()
{
	std::string field_string = "";

	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			if (field[i][j].get_state() == Alive)
				field_string += '1';
			else
				field_string += '0';
		}
	}
	return field_string;
}

void Strategy::set_field_string(int iter)
{
	history_map.erase(iter);
	std::string field_string = history_map.at(iter - 1);

	for (int i = 0; i < field.size(); i++)
	{
		for (int j = 0; j < field[0].size(); j++)
		{
			if ((field_string.c_str())[i * field.size() + j] - '0')
				field[i][j].set_state(Alive);
			else
				field[i][j].set_state(Dead);
		}
	}
}