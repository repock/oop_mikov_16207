﻿#include "game.h"
#include <crtdbg.h>
#include <iostream>

_CrtMemState study1 = { 0 };
_CrtMemState study2 = { 0 };
using namespace  std;
void game()
{
	Game a1;
	a1.start();
}
int main()
{
	_CrtMemCheckpoint(&study1);
	game();
	_CrtMemCheckpoint(&study2);
	_CrtMemState result = { 0 };
	if (_CrtMemDifference(&result, &study1, &study2))
	{
		_CrtMemDumpStatistics(&result);
		cout << "MEM LOSE: " << result.lTotalCount << endl;
	}
	else
		cout << "ALL IS OKEY";
}