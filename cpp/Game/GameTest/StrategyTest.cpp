﻿#include <gtest/gtest.h>
#include "../Game/strategy.h"


class StrategyTest : public testing::Test
{
	
};

//std::vector<std::vector<Cell>> *create_field(int width, int height)
//{
//	std::vector<std::vector<Cell>> *field = new std::vector<std::vector<Cell>>;
//
//	field->resize(height);
//
//	for (int i = 0; i < field->size(); i++)
//	{
//		((*field)[i]).resize(width);
//	}
//
//	return field;
//}
//
//TEST(StrategyTest, set) {
//
//	std::vector<vector<Cell>> *field = create_field(10, 10);
//	Strategy ex;
//	
//	char* b;
//	int i = 0;
//	
//	for (int j = 0; j < 5; j++)
//	{
//		ex.set(*field, i, j);
//		if ((*field)[i][j].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ("*", b);
//	}
//
//	delete field;
//}
//
//TEST(StrategyTest, clear) {
//	std::vector<vector<Cell>> *field = create_field(10, 10);
//	Strategy ex;
//
//	char* b;
//	int i = 0;
//
//	for (int j = 0; j < 5; j++)
//	{
//		ex.set(*field, i, j);
//		if ((*field)[i][j].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ("*", b);
//	}
//
//	for (int j = 0; j < 5; j++)
//	{
//		ex.clear(*field, i, j);
//		if ((*field)[i][j].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ(".", b);
//	}
//
//	delete field;
//}
//
//TEST(StrategyTest, reset)
//{
//	std::vector<vector<Cell>> *field = create_field(10, 10);
//	ConsolView view;
//	Strategy ex;
//
//	char* b;
//	int i = 0;
//
//	for (int j = 0; j < 5; j++)
//	{
//		ex.set(*field, i, j);
//		if ((*field)[i][j].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ("*", b);
//	}
//
//	ex.reset(*field, view);
//
//	for (int j = 0; j < 5; j++)
//	{
//		if ((*field)[i][j].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ(".", b);
//	}
//
//	delete field;
//}
//
//TEST(StrategyTest, step)
//{
//	std::vector<vector<Cell>> *field = create_field(10, 10);
//	Strategy ex;
//	ConsolView view;
//	std::map<int, string> history_map;
//
//	char* b;
//	int i = 1;
//
//	for (int j = 0; j < 3; j++)
//	{
//		ex.set(*field, i, j);
//	}
//
//	ex.step(*field, view, history_map, 1);
//
//	for (int j = 0; j < 3; j++)
//	{
//		if ((*field)[j][1].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ("*", b);
//	}
//	
//	delete field;
//}
//
//TEST(StrategyTest, back) {
//	std::vector<vector<Cell>> *field = create_field(10, 10);
//	Strategy ex;
//	ConsolView view;
//	std::map<int, string> history_map;
//
//	char* b;
//	int i = 0;
//
//	for (int j = 0; j < 3; j++)
//	{
//		ex.set(*field, i, j);
//	}
//
//	view.counter = 0;
//	ex.step(*field, view, history_map, 3);
//	ex.back(*field, view, history_map);
//
//	for (int j = 0; j < 3; j++)
//	{
//		if ((*field)[i][j].get_state() == Alive)
//			b = "*";
//		else b = ".";
//		ASSERT_STREQ("*", b);
//	}
//
//	delete field;
//}
//
//TEST(StrategyTest, counter) {
//	std::vector<vector<Cell>> *field = create_field(10, 10);
//	Strategy ex;
//	ConsolView view;
//	std::map<int, string> history_map;
//
//	view.counter = 0;
//	ex.step(*field, view, history_map, 5);
//
//	ASSERT_EQ(5, view.counter);
//	
//	delete field;
//}