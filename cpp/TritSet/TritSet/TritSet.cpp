﻿#include "Tritset.h"

tset::Trit tset::operator!(Trit t)
{
	switch (t) {
	case tset::FALSE:
		return tset::TRUE;
	case tset::UNKNOWN:
		return tset::UNKNOWN;
	case tset::TRUE:
		return tset::FALSE;
	} 
}

tset::Trit tset::operator|(Trit lv, Trit rv)
{
	tset::Trit res;
	switch (lv) {
	case tset::TRUE:
		res = tset::TRUE;
		break;
	case tset::UNKNOWN:
		if (rv == tset::TRUE) {
			res = tset::TRUE;
		}
		else {
			res = tset::UNKNOWN;
		}
		break;
	case tset::FALSE:
		res = rv;
		break;
	}
	return res;
}

tset::Trit tset::operator&(Trit lv, Trit rv)
{

	return !(!lv | !rv);
}

tset::TritSet::TritSet()
{
}

tset::TritSet::TritSet(size_t cap)
{
	size_t size1;
	size_t tritsUint = tritsInUint;

	if (cap % tritsUint) { // определим размер массива по заданной вместимости контейнера
		size1 = cap / tritsUint + 1;
	}
	else {
		size1 = cap / tritsUint;
	}

	//выделение памяти и инициализация
	this->arr = new uint[size1];
	for (int i = 0; i < size1; ++i) {
		arr[i] = 0;
	} 
	this->capacity = cap;
	this->size = size1;
}

tset::TritSet::TritSet(const TritSet & r)
{
	size = r.size;
	capacity = r.capacity;
	arr = new uint[size];
	std::memcpy( arr, r.arr, sizeof( int ) * size );
	//std::copy(&(arr[]) , &(arr[size]), &(r.arr[0])); почему не работает?
}


tset::TritSet::~TritSet()
{
	delete[] this->arr;
}

tset::TritSet::TritPtr tset::TritSet::operator[](size_t i)
{
	bool isExist = i < this->capacity; 
	size_t tritsUint = tritsInUint;

	size_t index = i / tritsUint;
	size_t pos = (tritsUint - ((i % tritsUint) + 1)) * 2; // число, на которое нужно сдвинуть вправо, чтобы дойти до нужного i-го трита

	return TritPtr(index, pos, this, isExist);
}

tset::Trit tset::TritSet::operator[](size_t i) const
{
	bool isExist = i < this->capacity; 
	size_t tritsUint = tritsInUint;

	size_t index = i / tritsUint;
	size_t pos = (tritsUint - ((i % tritsUint) + 1)) * 2; // число, на которое нужно сдвинуть вправо, чтобы дойти до нужного i-го трита

	return TritPtr(index, pos, const_cast<TritSet *>(this), true).getVal();
	
}

tset::TritSet tset::TritSet::operator~()
{
	 tset::TritSet res(this->capacity);

	for (int i = 0; i < capacity; ++i) {
		res[i] = !((*this)[i].val);
	}

	return res; 

}

tset::TritSet tset::TritSet::operator&(TritSet & rval)
{
	return binaryLogicOperator(*this, rval, tset::operator&);
}

tset::TritSet tset::TritSet::operator|(TritSet & rval)
{
	return binaryLogicOperator(*this, rval, tset::operator|);
}

tset::TritSet tset::TritSet::binaryLogicOperator(tset::TritSet & lval, tset::TritSet & rval, tset::Trit(*f)(tset::Trit, tset::Trit))
{
	size_t min_c = std::min(lval.capacity, rval.capacity);
	size_t max_c = std::max(lval.capacity, rval.capacity);
	tset::TritSet res(max_c);

	//выбор наибольшего по вместимости сета
	tset::TritSet * tmp;
	if (lval.capacity >= rval.capacity) {
		tmp = &lval;
	}
	else {
		tmp = &rval;

	//проход по сетам до тех пор, пока один из них(или оба) не кончатся
	}
	for (int i = 0; i < min_c; ++i) {
		res[i] = f(lval[i].val, rval[i].val);
	}
	//работа с оставшейся частью наибольшего сета
	for (int i = min_c; i < max_c; ++i) {
		res[i] = f((*tmp)[i].val, tset::UNKNOWN);
	}

	return res;
}

tset::TritSet & tset::TritSet::operator=(TritSet & r)
{
	//проверка на присваивание самому себе
	if (this != &r) {
		if (this->size != r.size) {
			this->resize(r.size);
			this->capacity = r.capacity; // изменяет вместимость, потому что ресайз сделал макс вместимость для этого размера
		}
		std::memcpy(arr, r.arr, sizeof(int) * r.size);
	}

	return (*this);
}

size_t tset::TritSet::cardinality(Trit t)
{
	size_t c = 0;
	size_t tmp = 0;
	if (t == tset::UNKNOWN) {
		for (int i = 0; i < capacity; ++i) {
			if ((*this)[i].val != tset::UNKNOWN) {
				c = tmp;
			}
			else {
				++tmp;
			}
		}
	}
	else {
		for (int i = 0; i < capacity; ++i) {
			if ((*this)[i].val == t) {
				++c;
			}
		}
	}
	return c;
}

void tset::TritSet::resize(size_t new_size)
{
	//принимает новый размер массива
	//устанавливает вместимость максимальную для этого размера
	size_t new_capacity = new_size * tritsInUint;
	//если размер массива не поменялся, увеличиваем вместимость до максимально возможной для этого массива
	//выделение памяти не требуется
	if (new_size == size) {
		capacity = new_capacity;
	}
	else {
		uint * new_arr = new uint[new_size];
		//старый массив копируется в новый
		size_t min_size = std::min(size, new_size);
		std::memcpy(new_arr, arr, sizeof(int) * min_size);

		//остальная часть зануляется, если размер был увеличен.
		if (new_size > size) {
			for (int i = size; i < new_size; ++i) {
				new_arr[i] = 0;
			}
		}

		delete[] arr;

		arr = new_arr;
		capacity = new_capacity;
		size = new_size;
	}
}

tset::TritSet::TritPtr::TritPtr(size_t i, size_t p, TritSet * s, bool e) :
	index(i), position(p), set(s), exist(e)
{
	if (!exist) {
		val = tset::UNKNOWN;
	}
	else {
		val = getTrit();
	}
}

tset::Trit tset::TritSet::TritPtr::getTrit() const
{
	// возвращает значение трита, лежащего по index, со сдвигом pos
	uint n = set->arr[index];
	uint mask = 0x00000003;
	uint trit = (n >> position) & mask;

	if (0 <= trit && trit <= 2) {
		return Trit(trit);
	}
	else {
		throw std::string("Can't get value");
	}
}

void tset::TritSet::TritPtr::setVal(Trit val)
{
	//устанавливает в контейнере значение val по данному сдвигу и индексу
	this->val = val;
	uint mask = ~(0x00000003 << position); // подводим маску к нужной позиции
	uint tmp = set->arr[index] & mask; // делаем дырку
	uint trit = val << position; //подгоняем к дырке
	set->arr[index] = trit | tmp;
}

tset::TritSet::TritPtr & tset::TritSet::TritPtr::operator=(Trit t)
{
	if (this->exist) {
		this->setVal(t);
	}
	else {
		if (t == tset::TRUE || t == tset::FALSE) {
			set->resize(index + 1); 
			this->setVal(t);
		}
	}

	return *this;
}

tset::TritSet::TritPtr & tset::TritSet::TritPtr::operator=(const TritSet::TritPtr & rval)
{
	return this->operator=(rval.getVal());
}

bool tset::operator==(const tset::TritSet & l, const tset::TritSet & r)
{
	if (&l != &r) {
		if (l.capacity != r.capacity || l.size != r.size)
		{
			return false;
		}
		else {
			for (int i = 0; i < l.size; ++i) {
				if (l.arr[i] != r.arr[i]) {
					return false;
				}
			}
		}
	}
		return true;
}

bool tset::operator!=(const tset::TritSet & r, const tset::TritSet & l)
{
	return !(r == l);
}

std::ostream & tset::operator<<(std::ostream & os, tset::TritSet & ts)
{
	for (int i = 0; i < ts.getCapacity(); ++i) {
		os << ts[i] << ' ';
	}
	return os;
}

std::ostream & tset::operator<<(std::ostream & os, tset::TritSet::TritPtr & tp)
{
	switch (tp.getVal())
	{
	case tset::FALSE:
		os << "FAlSE";
		break;
	case tset::TRUE:
		os << "TRUE";
		break;
	case tset::UNKNOWN:
		os << "UNKNOWN";
		break;
	default:
		break;
	}

	return os;
}

tset::Trit tset::TritSet::TritPtr::getVal() const
{
	return val;
}

tset::TritSet::TritPtr::operator tset::Trit()
{
	return this->getVal();
}

tset::uint tset::TritSet::getCapacity() const
{
	return capacity;
}

tset::uint tset::TritSet::getSize() const
{
	return size;
}

int tset::TritSet::last_changed_trit_index() const
{
	int last = 0;
	for(int i = 0; i < capacity; ++i)
	{
		if((*this)[i] != tset::UNKNOWN)
		{
			last = size;
		}
	}
	return last;
}

void tset::TritSet::shrink() {
	int old_cap = this->getCapacity();
	this->resize(this->last_changed_trit_index());
	this->capacity = old_cap;
}

std::unordered_map< tset::Trit, size_t, std::hash<int> > tset::TritSet::cardinality() {
	size_t capacity = size * tritsInUint;
	std::unordered_map< Trit, size_t, std::hash<int> > result;
	result[FALSE] = 0;
	result[UNKNOWN] = 0;
	result[TRUE] = 0;
	size_t tresult = 0;
	for (size_t i = 0; i < capacity; i++) {
		switch ((*this)[i])
		{
		case FALSE:
			result[FALSE]++;
			result[UNKNOWN] = tresult;
			break;
		case UNKNOWN:
			tresult++;
			break;
		case TRUE:
			result[TRUE]++;
			result[UNKNOWN] = tresult;
		}
	}
	return result;
}

size_t tset::TritSet::length() const
{
	size_t result = 0;
	for (int i = 0; i < capacity; ++i) {
		if ((*this)[i] != tset::UNKNOWN)
			result++;
	}
	return result + 1;
}
