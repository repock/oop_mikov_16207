﻿#include <iostream>
#include <unordered_map>
#include <algorithm>
#include <cstdlib>

namespace tset {

	using uint = unsigned int;

	enum Trit { FALSE = 1, UNKNOWN = 0, TRUE = 2 };

	Trit operator!(Trit);
	Trit operator|(Trit, Trit);
	Trit operator&(Trit, Trit);
	
	class TritSet
	{
	private:
		size_t capacity;
		size_t size;
		uint * arr;
		void resize(size_t new_size);
		size_t tritsInUint = sizeof(uint) * 4;
		int last_changed_trit_index() const;
	public:
		class TritPtr {
		private:
			friend class tset::TritSet;
			Trit val;
			size_t index;
			size_t position;
			TritSet * set;
			bool exist;

			TritPtr();
			TritPtr(size_t i, size_t p, TritSet * s, bool e);
			Trit getTrit() const;
			void setVal(Trit);
		public:
			Trit getVal() const;
			TritPtr& operator=(const Trit);
			TritPtr& operator=(const TritSet::TritPtr &);
			operator Trit ();
		};

		TritSet();
		TritSet(size_t size);
		TritSet(const TritSet &);
		~TritSet();

		uint getCapacity() const;
		uint getSize() const;

		void shrink();

		TritPtr operator[](size_t i);
		Trit operator[](size_t i) const;
		TritSet operator~();
		TritSet operator&(TritSet &);
		TritSet operator|(TritSet &);
		TritSet binaryLogicOperator(tset::TritSet & , tset::TritSet & , tset::Trit(*f)(tset::Trit, tset::Trit));
		TritSet & operator=(TritSet &);
		friend bool operator==(const tset::TritSet &, const tset::TritSet &);

		size_t cardinality(Trit);
		std::unordered_map< Trit, size_t, std::hash<int> > cardinality();
		size_t length() const;
	};

	bool operator!=(const tset::TritSet &, const tset::TritSet &);

	std::ostream& operator<<(std::ostream &, tset::TritSet &);
	std::ostream& operator<<(std::ostream &, tset::TritSet::TritPtr &);
}
