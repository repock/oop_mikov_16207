﻿#include <gtest/gtest.h>
#include "../TritSet/TritSet.h"

using namespace tset; 

const uint size_of_TRIT = 2;
const uint capacity_of_TRIT = sizeof(uint) * 8 / size_of_TRIT;

class TritSetTest : public testing::Test {

};

TEST_F(TritSetTest, getCapacity) {
	TritSet x(10), y(100), z(1000);
	EXPECT_EQ(10, x.getCapacity());
	EXPECT_EQ(100, y.getCapacity());
	EXPECT_EQ(1000, z.getCapacity());
}

TEST_F(TritSetTest, no_alloc) {
	TritSet set(10);
	set[1000000] = UNKNOWN;
	EXPECT_EQ(10, set.getCapacity());
	set[160] = UNKNOWN;
	EXPECT_EQ(10, set.getCapacity());
}

 TEST_F(TritSetTest, alloc) {
	TritSet set(3);
	set[48] = UNKNOWN;
	EXPECT_EQ(3, set.getCapacity());
	set[48] = FALSE;
	EXPECT_EQ(4, set.getSize());
	set[101] = TRUE;
	EXPECT_EQ(101 / capacity_of_TRIT + 1, set.getSize());
} 

TEST_F(TritSetTest, NOT)
{
	TritSet set(5);
	set[0] = FALSE;
	set[1] = UNKNOWN;
	set[2] = TRUE;
	EXPECT_EQ(TRUE, !set[0]);
	EXPECT_EQ(UNKNOWN, !set[1]);
	EXPECT_EQ(FALSE, !set[2]);
	set[7] = TRUE;
	set[8] = UNKNOWN;
	set[9] = FALSE;
	set[30] = FALSE;
	set[31] = UNKNOWN;
	set[32] = TRUE;
	TritSet result_set(5);
	result_set[0] = TRUE;
	result_set[1] = UNKNOWN;
	result_set[2] = FALSE;
	result_set[7] = FALSE;
	result_set[8] = UNKNOWN;
	result_set[9] = TRUE;
	result_set[30] = TRUE;
	result_set[31] = UNKNOWN;
	result_set[32] = FALSE;
	bool result = result_set == (~set);
	EXPECT_EQ(true, result);
}

TEST_F(TritSetTest, AND)
{
	TritSet set1(3);
	set1[0] = TRUE;
	set1[1] = UNKNOWN;
	set1[2] = FALSE;
	EXPECT_EQ(TRUE, set1[0] & set1[0]);
	EXPECT_EQ(UNKNOWN, set1[0] & set1[1]);
	EXPECT_EQ(UNKNOWN, set1[1] & set1[1]);
	EXPECT_EQ(FALSE, set1[0] & set1[2]);
	EXPECT_EQ(FALSE, set1[1] & set1[2]);
	EXPECT_EQ(FALSE, set1[2] & set1[2]);
	set1[7] = TRUE;
	set1[8] = UNKNOWN;
	set1[9] = FALSE;
	set1[30] = FALSE;
	set1[31] = UNKNOWN;
	set1[32] = TRUE;
	TritSet set2(3);
	set2[0] = UNKNOWN;
	set2[1] = TRUE;
	set2[2] = FALSE;
	set2[7] = TRUE;
	set2[8] = FALSE;
	set2[9] = TRUE;
	set2[30] = UNKNOWN;
	set2[31] = TRUE;
	set2[32] = FALSE;
	TritSet result_set(3);
	result_set[0] = UNKNOWN;
	result_set[1] = UNKNOWN;
	result_set[2] = FALSE;
	result_set[7] = TRUE;
	result_set[8] = FALSE;
	result_set[9] = FALSE;
	result_set[30] = FALSE;
	result_set[31] = UNKNOWN;
	result_set[32] = FALSE;
	bool result = result_set == (set1 & set2);
	EXPECT_EQ(true, result);
}

TEST_F(TritSetTest, OR)
{
	TritSet set1(5);
	set1[0] = TRUE;
	set1[1] = UNKNOWN;
	set1[2] = FALSE;
	EXPECT_EQ(TRUE, set1[0] | set1[0]);
	EXPECT_EQ(TRUE, set1[0] | set1[1]);
	EXPECT_EQ(TRUE, set1[0] | set1[2]);
	EXPECT_EQ(UNKNOWN, set1[1] | set1[1]);
	EXPECT_EQ(UNKNOWN, set1[1] | set1[2]);
	EXPECT_EQ(FALSE, set1[2] | set1[2]);
	set1[7] = TRUE;
	set1[8] = UNKNOWN;
	set1[9] = FALSE;
	set1[30] = FALSE;
	set1[31] = UNKNOWN;
	set1[32] = TRUE;
	TritSet set2(5);
	set2[0] = UNKNOWN;
	set2[1] = FALSE;
	set2[2] = TRUE;
	set2[7] = FALSE;
	set2[8] = TRUE;
	set2[9] = UNKNOWN;
	set2[30] = FALSE;
	set2[31] = UNKNOWN;
	set2[32] = FALSE;
	TritSet result_set(5);
	result_set[0] = TRUE;
	result_set[1] = UNKNOWN;
	result_set[2] = TRUE;
	result_set[7] = TRUE;
	result_set[8] = TRUE;
	result_set[9] = UNKNOWN;
	result_set[30] = FALSE;
	result_set[31] = UNKNOWN;
	result_set[32] = TRUE;
	bool result = result_set == (set1 | set2);
	EXPECT_EQ(true, result);
}

TEST_F(TritSetTest, equal_or_not)
{
	TritSet set(5), set1(5), set2(5), set3(6);
	size_t size = set.getCapacity() * capacity_of_TRIT;
	size_t size3 = set3.getCapacity() * capacity_of_TRIT;
	for (size_t i = 0; i < size; i++)
	{
		set[i] = set1[i] = TRUE;
		set2[i] = FALSE;
	}
	bool result1 = set == set1;
	bool result2 = set != set2;
	bool result3 = set != set3;
	EXPECT_EQ(true, result1, result2, result3);
} 

TEST_F(TritSetTest, size_of_result) {
	TritSet set1, set2;
	set1[333] = TRUE;
	set2[666] = TRUE;
	TritSet result_set = set1 & set2;
	EXPECT_EQ(666 / capacity_of_TRIT + 1, result_set.getSize());
	set1[999] = FALSE;
	result_set = set2 | set1;
	EXPECT_EQ(999 / capacity_of_TRIT + 1, result_set.getSize());
	set2[1488] = FALSE;
	result_set = ~set2;
	EXPECT_EQ(1488 / capacity_of_TRIT + 1, result_set.getSize());
}

TEST_F(TritSetTest, cardinality) {
	TritSet set(2);
	set[0] = TRUE;
	set[2] = TRUE;
	set[5] = TRUE;
	set[9] = TRUE;
	set[11] = TRUE;
	set[24] = TRUE;
	set[3] = FALSE;
	set[6] = FALSE;
	set[10] = FALSE;
	set[18] = FALSE;
	set[27] = FALSE;
	EXPECT_EQ(6, set.cardinality(TRUE));
	EXPECT_EQ(5, set.cardinality(FALSE));
	EXPECT_EQ(17, set.cardinality(UNKNOWN));
}

TEST_F(TritSetTest, cardinality_map) {
	TritSet set(2);
	set[0] = TRUE;
	set[2] = TRUE;
	set[5] = TRUE;
	set[9] = TRUE;
	set[24] = TRUE;
	set[3] = FALSE;
	set[10] = FALSE;
	set[18] = FALSE;
	set[27] = FALSE;
	std::unordered_map<Trit, size_t, std::hash<int> > map = set.cardinality();
	EXPECT_EQ(5, map[TRUE]);
	EXPECT_EQ(4, map[FALSE]);
	EXPECT_EQ(19, map[UNKNOWN]);
}

TEST_F(TritSetTest, copy)
{
	TritSet set(5);
	set[0] = TRUE;
	set[1] = UNKNOWN;
	set[2] = FALSE;
	set[7] = TRUE;
	set[8] = UNKNOWN;
	set[9] = FALSE;
	set[30] = FALSE;
	set[31] = UNKNOWN;
	set[32] = TRUE;
	TritSet copy_set(set);
	bool result = set == copy_set;
	EXPECT_EQ(true, result);
} 

TEST_F(TritSetTest, shrink) {
	TritSet set1;
	for (int i = 0; i<9; ++i)
	{
		set1[i] = TRUE;
	}
	set1.shrink();
	EXPECT_EQ(16 * (10 / capacity_of_TRIT + 1), set1.getCapacity());
	EXPECT_EQ((10 / capacity_of_TRIT + 1), set1.getSize());
	set1[15] = TRUE;
	set1.shrink();
	EXPECT_EQ(16*(15 / capacity_of_TRIT + 1), set1.getCapacity());
	EXPECT_EQ((15 / capacity_of_TRIT + 1), set1.getSize());
	set1[1000000] = FALSE;
	set1.shrink();
	EXPECT_EQ(16*(1000000 / capacity_of_TRIT + 1), set1.getCapacity());
	EXPECT_EQ(1000000 / capacity_of_TRIT + 1, set1.getSize());
}

TEST_F(TritSetTest, length) {
	TritSet set;
	set[666] = UNKNOWN;
	EXPECT_EQ(1, set.length());
	set[777] = FALSE;
	EXPECT_EQ(2, set.length());
	for(int i = 0; i<999;++i)
	{
		set[i] = TRUE;
	}
	EXPECT_EQ(1000, set.length());
}

