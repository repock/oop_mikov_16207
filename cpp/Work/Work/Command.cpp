#include "Command.h"

wrk::Command::Command()
{
	commands["readfile"] = new Readfile;
	commands["writefile"] = new Writefile;
	commands["grep"] = new Grep;
	commands["sort"] = new Sort;
	commands["replace"] = new Replace;
	commands["dump"] = new Dump;
}

wrk::Command::~Command()
{
	std::map<std::string, wrk::Worker *>::iterator it = commands.begin();
	for (; it != commands.end(); ++it) {
		delete it->second;
	}
	clear();
}

void wrk::Command::setExecutionOrder(std::vector<int> e)
{
	executionOrder = e;
}

bool wrk::Command::isExist(const std::string & id)
{
	std::map<std::string, wrk::Worker *>::iterator it = commands.find(id);
	if (it != commands.end()) {
		return true;
	}
	return false;
}

void wrk::Command::addCommand(int number, std::string name, std::vector<std::string> args)
{
	if (!isExist(name)) {
		throw err::WorkerError("unexcepted command");
	}
	mapCmd[number] = std::pair<wrk::Worker*, std::vector<std::string>>(commands[name], args);
}

void wrk::Command::clear()
{
	mapCmd.clear();
	executionOrder.clear();
}

void wrk::Command::executeAll()
{
	if (mapCmd.empty()) {
		return;
	}
	if (mapCmd[executionOrder[0]].first->getName() != "readfile") {
		throw err::ExecutionError("Incorrect first command of the block (supposed to be \"readfile\")");
	}
	if (mapCmd[executionOrder[executionOrder.size() - 1]].first->getName() != "writefile") {
		throw err::ExecutionError("Incorrect last command of the block (supposed to be \"writefile\")");
	}

	TextCont text;
	for (int i = 0; i < executionOrder.size(); ++i) {
		if (i != 0 && mapCmd[executionOrder[i]].first->getName() == "readfile") {
			throw err::ExecutionError("\"readfile\" is not supposed to be in the middle of the block");
		}
		if (i != executionOrder.size() - 1 && mapCmd[executionOrder[i]].first->getName() == "writefile") {
			throw err::ExecutionError("\"writefile\" is not supposed to be in the middle of the block");
		}
		if (!mapCmd[executionOrder[i]].first->isArgsValid(mapCmd[executionOrder[i]].second)) {
			throw err::ExecutionError("Invalid args for command " +
				std::to_string(i) + std::string(" = ") + mapCmd[executionOrder[i]].first->getName());
		}
		mapCmd[executionOrder[i]].first->execute(text, mapCmd[executionOrder[i]].second);
	}
}