#pragma once

#include"Worker.h"
#include"Factory.h"

#include<string>

namespace wrk {

	class WorkerF {
	public:
		WorkerF();
		Worker * getWorker(const std::string &);
	private:
		Factory<Worker, std::string> factory;
		void init();
	};

}