#pragma once

#include"Worker.h"

#include<string>

template<typename Base>
class AbstractCreator {
public:
	virtual Base * create() = 0;
	virtual ~AbstractCreator() {};
};

template <typename Base, typename T>
class Creator : public AbstractCreator <Base> {
public:
	Base * create() override { return new T; };
};




template<typename Base, typename TypeId>
class Factory {
public:
	typedef std::map<TypeId, AbstractCreator<Base> *> factoryMap;

	template<typename T>

	void add(const TypeId &);
	Base * create(const TypeId &);
	bool isExist(const TypeId &);
	~Factory();
private:
	factoryMap map;
};

template<typename Base, typename TypeId>
template<typename T>
inline void Factory<Base, TypeId>::add(const TypeId & id)
{
	if (isExist(id)) {
		return;
	}
	map[id] = new Creator<Base, T>;
}


template<typename Base, typename TypeId>
inline Base * Factory<Base, TypeId>::create(const TypeId & id)
{
	if (isExist(id)) {
		return map[id]->create();
	}
	else {
		return nullptr;
	}
}


template<typename Base, typename TypeId>
inline bool Factory<Base, TypeId>::isExist(const TypeId & id)
{
	factoryMap::iterator it = map.find(id);
	if (it != map.end()) {
		return true;
	}
	return false;
}

template<typename Base, typename TypeId>
inline Factory<Base, TypeId>::~Factory()
{
	factoryMap::iterator it = map.begin();
	for (; it != map.end(); ++it) {
		delete it->second;
	}
}