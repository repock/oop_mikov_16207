#pragma once

#include<vector>
#include<string>

namespace prs {
	class Parserargs {
	public:
		virtual bool isArgsValid(std::vector<std::string> & args) = 0;
		virtual ~Parserargs() {};
	};

	class NoParserargs : public Parserargs {
	public:
		virtual bool isArgsValid(std::vector<std::string> & args) override;
	};

	class OneStrParserargs : public Parserargs {
	public:
		virtual bool isArgsValid(std::vector<std::string> & args) override;
	};

	class TwoStrParserargs : public Parserargs {
	public:
		virtual bool isArgsValid(std::vector<std::string> & args) override;
	};
}