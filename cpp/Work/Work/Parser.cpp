#include "Parser.h"


void prs::Parser::parse(wrk::Command & cmdCont, std::ifstream & filename) const
{
	if (isStreamEmpty(filename)) {
		return;
	}
	std::vector<int> orderOfCommands;
	std::string tmpStr;
	cmdCont.clear();

	try {
		fillCommand(cmdCont, filename);
	}
	catch (err::ParserError & ) {
		throw;
	}

	try {
		std::getline(filename, tmpStr);
		orderOfCommands = getExecutionOrder(tmpStr);
	}
	catch (std::exception & ) {
		throw err::ParserError("Missing or incorrect struct's description");
	}
	cmdCont.setExecutionOrder(orderOfCommands);

}

void prs::Parser::fillCommand(wrk::Command & cmdCont, std::ifstream & file) const
{
	std::string tmpStr;
	std::vector<std::string> tmpVec;
	int tmpInt;
	std::getline(file, tmpStr);
	if (tmpStr != prs::keywordBlockBegin) {
		throw err::ParserError("Expected the keyword of the beginning of the block");
	}
	while (std::getline(file, tmpStr) && tmpStr != prs::keywordBlockEnd) {
		tmpVec = split(tmpStr);
		if (!isNonNegInt(tmpVec[0])) {
			throw err::ParserError("Exepted non negative number of execution");
		}
		if (tmpVec[1] != std::string("=")) {
			throw err::ParserError("Exepted \'=\'");
		}

		if (!cmdCont.isExist(tmpVec[2])) {
			throw err::ParserError("Unknown command: \'" + tmpVec[2] + '\'');
		}
		tmpInt = strsToInts({ tmpVec[0] })[0];
		tmpStr = tmpVec[2];
		tmpVec.erase(tmpVec.begin(), tmpVec.begin() + 3);	
		cmdCont.addCommand(tmpInt, tmpStr, tmpVec);
	}
	if (tmpStr != prs::keywordBlockEnd) {
		throw err::ParserError("Expected the keyword of the end of the block");
	}
}



std::vector<std::string> prs::Parser::splitByStr(const std::string & str, const std::string & sep) const
{
	std::vector<std::string> res;
	int lastIndex = 0;
	int tmp = -1;
	while ((tmp = str.find(sep, tmp + 1)) != std::string::npos) {
		res.push_back(str.substr(lastIndex, tmp - lastIndex));
		lastIndex = tmp + sep.size(); 
	}
	if (lastIndex < str.size()) {
		res.push_back(str.substr(lastIndex, str.size() - lastIndex));
	}
	return res;
}

std::vector<int> prs::Parser::strsToInts(const std::vector<std::string>& vecStr) const
{
	std::vector<int> res;
	for (int i = 0; i < vecStr.size(); ++i) {
		if (!isNonNegInt(vecStr[i])) {
			throw err::ParserError("Exepted non negative ints in struct's description");
		}
		res.push_back(std::stoi(vecStr[i]));
	}
	return res;
}

bool prs::Parser::isNonNegInt(const std::string & str) const
{
	return (str.find_first_not_of("0123456789") == std::string::npos);
}

std::vector<int> prs::Parser::getExecutionOrder(const std::string & str) const
{
	return strsToInts(splitByStr(str, " -> "));
}

bool prs::Parser::isStreamEmpty(std::ifstream& pfile) const
{
	return pfile.peek() == std::ifstream::traits_type::eof();
}


std::vector<std::string> prs::Parser::split(const std::string & str, char spl) const
{
	std::vector<std::string> res;
	std::string tmp;
	for (int i = 0; i < str.size(); ++i) {
		if (str[i] == spl) {
			if (!tmp.empty()) {
				res.push_back(tmp);
				tmp = "";
			}
		}
		else {
			tmp += str[i];
		}
	}
	if (!tmp.empty()) {
		res.push_back(tmp);
	}
	return res;
}