#include "WorkerF.h"

wrk::WorkerF::WorkerF()
{
	init();
}

wrk::Worker * wrk::WorkerF::getWorker(const std::string & id)
{
	return factory.create(id);
}

void wrk::WorkerF::init()
{
	factory.add<Readfile>("readfile");
	factory.add<Writefile>("writefile");
	factory.add<Grep>("grep");
	factory.add<Sort>("sort");
	factory.add<Replace>("replace");
	factory.add<Dump>("dump");
}