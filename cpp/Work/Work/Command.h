#pragma once

#include"Worker.h"
#include"TextCont.h"
#include"Exception.h"

#include<map>
#include<vector>
#include<utility>

namespace wrk {
	class Worker;
	typedef std::map<int, std::pair<Worker*, std::vector<std::string>>> ExecutionMap;

	class Command {
	public:
		Command();
		~Command();
		void setExecutionOrder(std::vector<int> e);
		bool isExist(const std::string & s);
		void addCommand(int number, std::string name, std::vector<std::string> args);
		void clear();
		void executeAll();

	private:
		std::map<std::string, wrk::Worker *> commands;
		ExecutionMap mapCmd;
		std::vector<int> executionOrder;
	};
}