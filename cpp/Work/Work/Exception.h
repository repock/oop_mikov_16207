#pragma once

#include <exception>
#include <string>

namespace err {

	class BaseError : public std::exception {
	public:
		BaseError(const char * c) : str(c) {};
		BaseError(const std::string & s) : str(s) {};
		virtual ~BaseError() {};
		virtual const char * what() const noexcept { return str.c_str(); };
	private:
		std::string str;
	};

	class WorkerError : public BaseError {
	public:
		WorkerError(const char * c) : BaseError(c) {};
		WorkerError(const std::string & s) : BaseError(s) {};
	};

	class ParserError : public BaseError {
	public:
		ParserError(const char * c) : BaseError(c) {};
		ParserError(const std::string & s) : BaseError(s) {};
	};

	class ExecutionError : public BaseError {
	public:
		ExecutionError(const char * c) : BaseError(c) {};
		ExecutionError(const std::string & s) : BaseError(s) {};
	};

	class ModelError : public BaseError {
	public:
		ModelError(const char * c) : BaseError(c) {};
		ModelError(const std::string & s) : BaseError(s) {};
	};

}