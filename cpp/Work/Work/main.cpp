#include"Worker.h"
#include"Parser.h"
#include"Exception.h"
#include"Command.h"

#include<iostream>
#include<fstream>
#include<vector>


int main(int argc, char ** argv)
{
	if (argc < 2) {
		std::cout << "Input file name excepted\n";
		return 1;
	}
	std::ifstream file(argv[1]);
	if (!file.is_open()) {
		std::cout << "Can not open \'" + std::string(argv[1]) + "\'";
		return 1;
	}

	wrk::Command c;
	prs::Parser p;
	while (file.peek() != std::ifstream::traits_type::eof()) {
		try {
			c.clear();
			p.parse(c, file);
			c.executeAll();
		}
		catch (err::BaseError & e) {
			std::cout << e.what() << '\n';
			return 1;
		}
		catch (std::exception & e) {
			std::cout << e.what() << '\n';
			return 1;
		}
	}
	std::cout << "Done.\n";
}