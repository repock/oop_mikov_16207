#pragma once

#include "Worker.h"
#include"Parserargs.h"
#include"TextCont.h"
#include"Exception.h"

#include<map>
#include<fstream>
#include<vector>
#include<string>

namespace wrk {

	class Worker {
	public:
		Worker(prs::Parserargs * p, std::string s) : parser(p), name(s) {}
		virtual ~Worker();
		virtual void execute(TextCont & text, std::vector<std::string> args) = 0;
		bool isArgsValid(std::vector<std::string> args);
		std::string getName() { return name; };
	private:
		prs::Parserargs * parser;
		std::string name;
	};

	class Readfile : public Worker {
	public:
		Readfile() : Worker(new prs::OneStrParserargs, "readfile") {};
		void execute(TextCont & text, std::vector<std::string> args) override;
	};

	class Writefile : public Worker {
	public:
		Writefile() : Worker(new prs::OneStrParserargs, "writefile") {};
		void execute(TextCont & text, std::vector<std::string> args) override;
	};

	class Grep : public Worker {
	public:
		Grep() : Worker(new prs::OneStrParserargs, "grep") {};
		void execute(TextCont & text, std::vector<std::string> args) override;
	};

	class Sort : public Worker {
	public:
		Sort() : Worker(new prs::NoParserargs, "sort") {};
		void execute(TextCont & text, std::vector<std::string> args) override;
	};

	class Replace : public Worker {
	public:
		Replace() : Worker(new prs::TwoStrParserargs, "replace") {};
		void execute(TextCont & text, std::vector<std::string> args) override;
	};

	class Dump : public Worker {
	public:
		Dump() : Worker(new prs::OneStrParserargs, "dump") {};
		void execute(TextCont & text, std::vector<std::string> args) override;
	};
}