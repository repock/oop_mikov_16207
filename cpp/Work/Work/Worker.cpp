#include "Worker.h"

#include<iostream>

wrk::Worker::~Worker()
{
	delete parser;
}

bool wrk::Worker::isArgsValid(std::vector<std::string> args)
{
	return	parser->isArgsValid(args);
}


void wrk::Readfile::execute(TextCont & text, std::vector<std::string> args)
{
	if (!isArgsValid(args)) {
		throw err::ExecutionError("Invalid args for command \'readfile\'");
	}

	std::ifstream file(args[0].c_str());
	if (!file.is_open()) {
		throw err::ExecutionError("Cant open the file: \'" + args[0] + "\'");
	}
	std::string tmp;
	while (file.peek() != std::ifstream::traits_type::eof()) {
		std::getline(file, tmp);
		text.text.push_back(tmp);
	}
}

void wrk::Writefile::execute(TextCont & text, std::vector<std::string> args)
{
	if (!isArgsValid(args)) {
		throw err::ExecutionError("Invalid args for command \'writefile\'");
	}

	std::ofstream file(args[0].c_str());
	if (!file.is_open()) {
		throw err::ExecutionError("Cant open the file: \'" + args[0] + "\'");
	}
	std::string tmp;
	while (!text.text.empty()) {
		file << text.text.front() << '\n';
		text.text.pop_front();
	}
}

void wrk::Grep::execute(TextCont & text, std::vector<std::string> args)
{
	if (!isArgsValid(args)) {
		throw err::ExecutionError("Invalid args for command \'grep\'");
	}

	std::list<std::string>::iterator it = text.text.begin();
	while (it != text.text.end()) {
		if (it->find(args[0]) == std::string::npos) {
			it = text.text.erase(it);
		}
		else {
			++it;
		}
	}
}

void wrk::Sort::execute(TextCont & text, std::vector<std::string> args)
{
	if (!isArgsValid(args)) {
		throw err::ExecutionError("Invalid args for command \'sort\'");
	}
	text.text.sort();
}

void wrk::Replace::execute(TextCont & text, std::vector<std::string> args)
{
	if (!isArgsValid(args)) {
		throw err::ExecutionError("Invalid args for command \'replace\'");
	}

	std::string::size_type i = 0;
	std::list<std::string>::iterator it = text.text.begin();
	for (; it != text.text.end(); ++it) {
		while (it->find(args[0], i) != std::string::npos) {
			i = it->find(args[0], i);
			it->replace(i, args[0].length(), args[1]);
			i += args[1].length();
		}
		i = 0;
	}
}

void wrk::Dump::execute(TextCont & text, std::vector<std::string> args)
{
	if (!isArgsValid(args)) {
		throw err::ExecutionError("Invalid args for command \'dump\'");
	}

	std::ofstream file(args[0].c_str());
	if (!file.is_open()) {
		throw err::ExecutionError("Cant open the file: \'" + args[0] + "\'");
	}

	std::list<std::string>::iterator it = text.text.begin();
	for (; it != text.text.end(); ++it) {
		file << *it << '\n';
	}
}
