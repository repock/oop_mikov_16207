#include "Parserargs.h"


bool prs::NoParserargs::isArgsValid(std::vector<std::string>& args)
{
	return !args.size();
}

bool prs::OneStrParserargs::isArgsValid(std::vector<std::string>& args)
{
	return !(args.size() != 1 || args[0].empty());
}

bool prs::TwoStrParserargs::isArgsValid(std::vector<std::string>& args)
{
	return !(args.size() != 2 || args[0].empty() || args[1].empty());
}

