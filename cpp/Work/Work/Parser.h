#pragma once

#include"Command.h"
#include"Exception.h"
#include"Typesetting.h"

#include<fstream>
#include<string>
#include<vector>

namespace wrk {
	class Command;
}

namespace prs {

	static class Parser {
	public: 
		void parse(wrk::Command &, std::ifstream & filename) const;
		std::vector<std::string> split(const std::string & str, char spl = ' ') const;
		std::vector<std::string> splitByStr(const std::string & str, const std::string & spl) const;
		std::vector<int> strsToInts(const std::vector<std::string> & vecStr) const;
		bool isNonNegInt(const std::string & str) const;
		std::vector<int> getExecutionOrder(const std::string & str) const;
		bool isStreamEmpty(std::ifstream & pfile) const;
	private:
		void fillCommand(wrk::Command &, std::ifstream &) const;
	};

}