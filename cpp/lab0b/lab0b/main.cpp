#include "Header.h"

using namespace std;

int main(int argc, char *argv[])
{
	string IN_FILE_ARG_INDEX = argv[1];
	string OUT_FILE_ARG_INDEX = argv[2];

	list <string> spisok;
	ifstream inpotok(IN_FILE_ARG_INDEX);
	
	setlocale(LC_ALL, "rus");


	
	if (argc != 3)
	{
		cout << "�������� ���������� ����������, ������ �����: <��� �������� �����> <��� ��������� �����>" << endl;
		return EXIT_FAILURE;
	}

	ofstream outpotok(OUT_FILE_ARG_INDEX);
	string vremennai;

	
	if (!inpotok)
	{
		cout << "���������� ������� ������� ����" << endl;
		return EXIT_FAILURE;
	}


	if (!outpotok)
	{
		cout << "��������� ��������� �����" << endl;
		return EXIT_FAILURE;
	}

	while (getline(inpotok, vremennai))
		spisok.push_back(vremennai);

	sort::sort_strings(spisok);

	while(!(spisok.empty()))
	{
		outpotok << spisok.front() << endl;
			spisok.pop_front();
	}

	inpotok.close();
	outpotok.close();

	return EXIT_SUCCESS;
}