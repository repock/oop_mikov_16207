#include <gtest/gtest.h>
#include "../lab2/HashTable.h"

class HashTableTest : public testing::Test
{

};

/* TEST_F(HashTableTest, TestAdd)
{
	EXPECT_EQ(42, HashTable::add(40, 2));
} */


TEST_F(HashTableTest, constructor)
{
	ASSERT_NO_THROW(HashTable a);
}

TEST_F(HashTableTest, copyConstructor_empty)
{
	HashTable temp;
	ASSERT_NO_THROW(HashTable a(temp));
}

TEST_F(HashTableTest, copyConstructor_notempty)
{
	HashTable temp;
	for (unsigned int i = 0; i<100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
	}
	ASSERT_NO_THROW(HashTable a(temp));
}

TEST_F(HashTableTest, assignmentOperator_empty)
{
	HashTable temp;
	ASSERT_NO_THROW(HashTable temp2 = temp);
}

TEST_F(HashTableTest, assignmentOperator_notempty)
{
	HashTable temp, temp1;
	for (unsigned int i = 0; i < 100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
	}

	ASSERT_NO_THROW(temp1 = temp);
	EXPECT_EQ(temp.size(), temp1.size());
}

TEST_F(HashTableTest, clear_empty)
{
	HashTable temp;
	ASSERT_NO_THROW(temp.clear());
	EXPECT_EQ(temp.table->size(), 17);
	EXPECT_EQ(temp.size(), 0);
}

TEST_F(HashTableTest, clear_notempty)
{
	HashTable temp;
	for (unsigned int i = 0; i < 100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
	}
	ASSERT_NO_THROW(temp.clear());
	EXPECT_EQ(temp.table->size(), 17);
	EXPECT_EQ(temp.size(), 0);
} 

TEST_F(HashTableTest, insert_empty)
{
	HashTable temp;
	EXPECT_TRUE(temp.insert("Mikov", { 19,27 }));
	EXPECT_EQ(temp.size(), 1);
	EXPECT_TRUE(temp.contains("Mikov"));
}

TEST_F(HashTableTest, insertTheSameKeys)
{
	HashTable temp;
	EXPECT_TRUE(temp.insert("Mikov", { 19,27 }));
	EXPECT_EQ(temp.size(), 1);
	EXPECT_TRUE(temp.insert("Mikov", { 34,59 }));
	EXPECT_TRUE(temp.contains("Mikov"));
	Value temp1 = temp.at("Mikov");
	EXPECT_EQ(temp1.age, 34);
	EXPECT_EQ(temp1.weight, 59);

}

TEST_F(HashTableTest, insertWithResize)
{
	HashTable temp;
	for (unsigned int i = 0; i<100; i++)
	{
		EXPECT_TRUE(temp.insert("Mikov" + std::to_string(i), { i, i }));
	}

	for (int i = 0; i<100; i++)
	{
		EXPECT_TRUE(temp.contains("Mikov" + std::to_string(i)));
	}

	EXPECT_EQ(temp.size(), 100);
	EXPECT_FALSE(temp.empty());
}

TEST_F(HashTableTest, erase_empty)
{
	HashTable temp;
	EXPECT_FALSE(temp.erase("Mikov"));
}

TEST_F(HashTableTest, erase)
{
	HashTable temp;
	for (unsigned int i = 0; i<100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
	}

	for (unsigned int i = 0; i<100; i++)
	{
		EXPECT_TRUE(temp.erase("Mikov" + std::to_string(i)));
	}

	EXPECT_EQ(temp.size(), 0);
	EXPECT_TRUE(temp.empty());
}

TEST_F(HashTableTest, at_notfound)
{
	HashTable temp;
	ASSERT_ANY_THROW(temp.at("Mikov"));
}

TEST_F(HashTableTest, at_found)
{
	HashTable temp;
	for (unsigned int i = 0; i<100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
	}

	for (unsigned int i = 0; i<100; i++)
	{
		ASSERT_NO_THROW(temp.at("Mikov" + std::to_string(i)));
	}
}

TEST_F(HashTableTest, operIndex_empty)
{
	HashTable temp;
	EXPECT_EQ(temp.size(), 0);
	Value temp1 = temp["Mikov"];
	EXPECT_EQ(temp1.age, 0);
	EXPECT_EQ(temp1.weight, 0);
	EXPECT_EQ(temp.size(), 1);
}

TEST_F(HashTableTest, operIndex_notempty)
{
	HashTable temp;
	for (unsigned int i = 0; i<100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
	}
	Value temp1 = temp["Mikov50"];
	EXPECT_EQ(temp1.age, 50);
	EXPECT_EQ(temp1.weight, 50);
}

TEST_F(HashTableTest, swap)
{
	HashTable temp, temp2;
	temp.insert("Mikov1", { 19,14 });
	temp2.insert("Mikov_1", { 25,28 });
	temp2.insert("Mikov_2", { 65,32 });
	temp.swap(temp2);
	EXPECT_EQ(temp.size(), 2);
	EXPECT_EQ(temp2.size(), 1);
	EXPECT_TRUE(temp.contains("Mikov_1"));
	EXPECT_TRUE(temp.contains("Mikov_2"));
	EXPECT_TRUE(temp2.contains("Mikov1"));
}

TEST_F(HashTableTest, comparisonOperator_1)
{
	HashTable temp, temp2;
	EXPECT_TRUE(temp == temp);
	for (unsigned int i = 0; i< 100; i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
		temp2.insert("Mikov" + std::to_string(i), { i, i });
	}

	EXPECT_TRUE(temp == temp2);
	EXPECT_FALSE(temp != temp2);

}

TEST_F(HashTableTest, comparisonOperator_2)
{
	HashTable temp, temp2;
	for (unsigned int i = 0; i<100;
		i++)
	{
		temp.insert("Mikov" + std::to_string(i), { i, i });
		temp2.insert("Mikov" + std::to_string(i), { i, i });
	}
	temp2.erase("Mikov99");
	EXPECT_FALSE(temp == temp2);
	EXPECT_TRUE(temp != temp2);

}