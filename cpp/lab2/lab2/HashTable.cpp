﻿#include "HashTable.h"
 

HashTable::HashTable()
{
	table = new HTVector(defaultCapacity);
	count = 0;
}

HashTable::~HashTable()
{
	HashTable::destroy(table, capacity);
	delete table;
}

void HashTable::destroy(HTVector *vect, size_t a)
{
	for (size_t i = 0; i < a; i++)
	{
		delete (*vect)[i];
		(*vect)[i] = 0;
	}
}


HashTable::HashTable(const HashTable &b)
{
	try
	{
		table = new HTVector(b.capacity);
		HashTable::copyvector(table, b);
	}
	catch (std::bad_alloc &)
	{
		HashTable::destroy(table, capacity);
		delete table;
		throw;
	}

	count = b.count;
}

void HashTable::copyvector(HTVector *firstVect, const HashTable &b)
{
	(*firstVect).resize(b.capacity);

	for (size_t i = 0; i < b.capacity; i++)
	{
		if (!(*b.table)[i])
		{
			continue;
		}

		try 
		{
			 (*firstVect)[i] = new HTList;

			for (HTList::iterator iter = ((*b.table)[i])->begin(); iter != ((*b.table)[i])->cend(); iter++)
			{
				(*firstVect)[i]->push_back(make_pair(iter->first, iter->second));
			}
		} 
		catch (std::bad_alloc &)
		{
			delete (*firstVect)[i];
			throw;
		}
	}
}

void HashTable::swap(HashTable& b) //перекидываем указатели, вместо стандартной побитовой
{
	std::swap(table, b.table);
	std::swap(count, b.count);
	std::swap(capacity, b.capacity);
}

HashTable &HashTable::operator=(const HashTable &b) //?
{
	HTVector *newTable = new HTVector(b.capacity);

	if (this == &b)
	{
		return *this;
	}

	try
	{
		HashTable::copyvector(newTable, b);
	}
	catch (std::bad_alloc &)
	{
		HashTable::destroy(newTable, b.capacity);
		delete newTable;
		throw;
	}

	HashTable::destroy(table, capacity);
	delete table;

	count = b.count;
	capacity = b.capacity;
	table = newTable;

	return (*this);
}

void HashTable::clear()
{
	HashTable::destroy(table, capacity);
	table->resize(defaultCapacity);
	capacity = defaultCapacity;
	count = 0;
}

bool HashTable::resize()
{
	size_t oldS = capacity;
	update(capacity);

	HTVector *newTable = 0;

	try
	{
		newTable = new HTVector(capacity);
	}
	catch (std::bad_alloc &)
	{
		capacity = oldS;
		throw;
	}

	for (size_t i = 0; i < oldS; i++)
	{
		if (!(*table)[i])
		{
			continue;
		}

		for (HTList::iterator iter = ((*table)[i])->begin(); iter != ((*table)[i])->cend(); iter++)
		{
			size_t index = HashTable::hash(iter->first);

			// поиск в листе
			if ((*newTable)[index])
			{
				(*newTable)[index]->push_back(make_pair(iter->first, iter->second));
				continue; // успешная вставка
			}

			// новый лист
			try
			{
				(*newTable)[index] = new HTList;
				(*newTable)[index]->push_back(make_pair(iter->first, iter->second));
			}
			catch (std::bad_alloc &)
			{
				HashTable::destroy(newTable, capacity);
				delete newTable;
				throw;
			}
		}
	}

	HashTable::destroy(table, oldS);
	delete table;

	table = newTable;
	return true;
}

size_t HashTable::update(size_t n)
{
	size_t newn = n * 2;
	size_t i;
	while (true)
	{
		newn++;

		for (i = 2; i <= newn / 2; i++)
		{
			if (newn % i == 0)
			{
				break;
			}
		}

		if (i > newn / 2)
		{
			break;
		}
	}

	return newn;
}

size_t HashTable::hash(const Key &str) const
{
	return (std::hash<std::string>()(str) % capacity);
}

bool HashTable::erase(const Key& k)
{
	size_t index = HashTable::hash(k);

	if (!(*table)[index])
	{
		return false;
	}

	HTList::iterator iter;

	if (newIter(&iter, (*table)[index], k))
	{
		(*table)[index]->erase(iter);
		count--;
		return true;
	}
	else
	{
		return false;
	}
}

bool HashTable::newIter(HTList::iterator *iter, HTList *list, const Key &name) const
{
	for ((*iter) = list->begin(); (*iter) != list->cend(); (*iter)++)
	{
		if (name == (*iter)->first)
		{
			return true;
		}
	}

	return false;
}

bool HashTable::insert(const Key& k, const Value& v)
{
	// изменить размер?
	if (100 * (count + 1) / capacity > max_overflow)
	{
		HashTable::resize();
	}

	size_t index = HashTable::hash(k);

	// новый лист
	if (!(*table)[index])
	{
		try
		{
			(*table)[index] = new HTList;
			(*table)[index]->push_back(make_pair(k, Value{ v.age, v.weight }));
			++count;
		}
		catch (std::bad_alloc &)
		{
			delete (*table)[index];
			(*table)[index] = 0;
			throw;
		}

		return true; // успешная вставка
	}


	// вставить в список
	HTList::iterator iter;

	if (newIter(&iter, (*table)[index], k))
	{
		iter->second = Value{ v.age, v.weight };
	}
	else
	{
		(*table)[index]->push_back(make_pair(k, Value{ v.age, v.weight }));
		count++;
	}

	return true; // успешная вставка
}

bool HashTable::contains(const Key& k) const
{
	size_t const index = HashTable::hash(k);

	if (!(*table)[index])
	{
		return false;
	}

	HTList::iterator iter;
	if (newIter(&iter, (*table)[index], k))
	{
		return true;
	}
	return false;
}

Value &HashTable::operator[](const Key &k)
{
	size_t const index = HashTable::hash(k);

	// новый лист
	if (!(*table)[index])
	{
		Value temp;
		HashTable::insert(k, temp);
		return temp;
	}

	// поиск в листе
	HTList::iterator iter;

	if (newIter(&iter, (*table)[index], k))
	{
		return iter->second;
	}
	else
	{
		(*table)[index]->push_back(std::make_pair(k, Value{ 0, 0 }));
		return (*table)[index]->back().second;
	}
}

Value& HashTable::at(const Key &name)
{
	size_t index = HashTable::hash(name);

	if (!(*table)[index])
	{
		throw (std::out_of_range(name));
	}

	HashTable::HTList::iterator iter;

	if (newIter(&iter, (*table)[index], name))
	{
		return iter->second;
	}
	else
	{
		throw (std::out_of_range(name));
	}
}

const Value& HashTable::at(const Key &name2) const
{
	size_t const index = HashTable::hash(name2);

	if (!(*table)[index])
	{
		throw (std::out_of_range(name2));
	}

	HashTable::HTList::iterator iter;

	if (newIter(&iter, (*table)[index], name2))
	{
		return iter->second;
	}
	else
	{
		throw (std::out_of_range(name2));
	}
}

size_t HashTable::size() const
{
	return count;
}

bool HashTable::empty() const
{
	if (count == 0)
		return true;
	else
		return false;
}

bool operator==(const HashTable &firstTable, const HashTable &secondTable)
{
	if (&firstTable == &secondTable)
	{
		return true;
	}

	if (firstTable.capacity != secondTable.capacity)
	{
		return false;
	}

	if (firstTable.count != secondTable.count)
	{
		return false;
	}

	for (size_t i = 0; i < firstTable.capacity; i++)
	{
		HashTable::HTList *first = (*firstTable.table)[i];
		HashTable::HTList *second = (*secondTable.table)[i];

		if (!first != !second) // сравнить наличие указателей, а не их значение
		{
			return false;
		}

		if (!(first || second))
		{
			continue;
		}

		if (first->size() != second->size())
		{
			return false;
		}

		HashTable::HTList::iterator iterfirst = first->begin();
		HashTable::HTList::iterator itersecond = second->begin();

		for (; iterfirst != first->cend(); iterfirst++, itersecond++)
		{
			if (iterfirst->first != itersecond->first || iterfirst->second.age != itersecond->second.age || iterfirst->second.weight != itersecond->second.weight)
			{
				return false;
			}
		}
	}

	return true;
}

bool operator!=(const HashTable &firstTable, const HashTable &secondTable)
{
	return !(firstTable == secondTable);
}