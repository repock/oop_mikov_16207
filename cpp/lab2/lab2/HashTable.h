﻿#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
#include <list>
#include <exception>


using Key = std::string;

struct Value {
	Value(unsigned age = 0, unsigned weight = 0)
		:age(age), weight(weight) {}
	unsigned age;
	unsigned weight;
};

class HashTable
{
	private:
		using HTPair = std::pair< Key, Value >;
		using HTList = std::list< HTPair >;
		using HTVector = std::vector< HTList *>;
		unsigned static const defaultCapacity = 17;
		unsigned capacity = defaultCapacity;
		/* HTVector *table; */
		unsigned count;
		unsigned static const max_overflow = 75; //%

		void destroy(HTVector *, size_t);
		void copyvector(HTVector *, const HashTable &b);
		bool resize();
		size_t update(size_t);
		size_t hash(const Key &) const;
	//задавать итератор, если содержит ключ
		bool newIter(HTList::iterator *, HTList *, const Key &) const;

	public:
		HTVector *table; 

		HashTable();
		~HashTable();

		HashTable(const HashTable& b);

		// Обменивает значения двух хэш-таблиц.
		void swap(HashTable& b);

		HashTable& operator=(const HashTable& b);

		// Очищает контейнер. Возвращает таблицу к начальному размеру
		void clear();

		// Удаляет элемент по заданному ключу. Возвращает true в случае успеха, //false - если элемент с данным ключом отсутствует в хэш-таблице
		bool erase(const Key& k);

		// Вставка в контейнер. Возвращаемое значение - успешность вставки.
		bool insert(const Key& k, const Value& v);

		// Проверка наличия значения по заданному ключу.
		bool contains(const Key& k) const;

		// Возвращает значение по ключу. Небезопасный метод.
		// В случае отсутствия ключа в контейнере следует вставить в контейнер
		// значение Value, созданное конструктором по умолчанию и вернуть ссылку на //него. 
		Value& operator[](const Key& k);

		// Возвращает значение по ключу. Бросает исключение при неудаче.
		Value& at(const Key& k);

		//Аналогичный предыдущему метод, используемые константыми хэш-таблицами
		const Value& at(const Key& k) const;

		//Возвращает размер хэш-таблицы (количество элементов в ней)
		size_t size() const;

		//Возвращает true - если таблица пустая, иначе - false
		bool empty() const;

		//Пара операторов для проверки на равенство хэш-таблиц. Объяснить, почему //данные методы являются друзьями, а не членами класса HashTable
		friend bool operator==(const HashTable & a, const HashTable & b);
		friend bool operator!=(const HashTable & a, const HashTable & b);
};